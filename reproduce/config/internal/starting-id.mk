# ID to start counting the objects.
#
# This ID will also be used in the final labeled image. This pipeline
# was designed to add to the Rafelski et al. 2015 catalog and
# segmentation map, so the starting ID is 60000 and the final
# segmentation map will be added to that of Rafelski et al. 2015.
#
# If you don't plan to use the Rafelski segmentation map, then you can
# set this value to 0, ignore the segmentation map in the
# `reproduce/output' directory and use the following segmentation map:
# `$(BDIR)/fits/segmentation/nc-and-aper.fits.gz'
startingid=60000
