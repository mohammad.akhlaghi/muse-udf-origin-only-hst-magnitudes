# Random number generator type
#
# Random numbers are necessary to generate the upper limit magnitude
# for each object. For each filter we have a fixed seed that is set in
# `src/genseed.sh' and here we set the random number generator type.
rngtype=mt19937
