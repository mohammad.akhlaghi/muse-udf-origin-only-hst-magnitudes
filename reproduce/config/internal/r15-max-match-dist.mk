# Maximum acceptable distance to match Rafelski+2015 and NoiseChisel
# catalogs IN PIXELS.
#
# The image has a resolution of 0.06 arcseconds/pixel
# 3 pixels is thus 0.18 arcseconds = 0.18/3600 deg = 0.00005 deg
r15maxmatchdist=0.00005
