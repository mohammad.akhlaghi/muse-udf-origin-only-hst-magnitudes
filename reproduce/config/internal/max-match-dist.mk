# Maximum acceptable distance to match catalogs in degrees.
#
# The image has a resolution of 0.06 arcseconds/pixel
# 5 pixels is thus 0.3 arcseconds = 0.3/3600 deg = 0.000083333 deg
maxmatchdist=0.000083333
