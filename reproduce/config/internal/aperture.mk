# Radius of the aperture in pixels
#
# Image resolution is 0.06 arcsec/pixel so an aperture of 0.25
# arcseconds is 0.25/0.06=4.16666 pixels
apertureradius=4.16666
