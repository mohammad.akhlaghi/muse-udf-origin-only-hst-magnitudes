# Radius of the aperture in pixels
#
# Image resolution is 0.06 arcsec/pixel so an aperture of radius 0.2
# arcseconds is 0.2/0.06=3.333333 pixels
aperturewhtradius=3.333333
