# XDF vertices
# ============
#
# This is the deep XDF region with mostly uniform depth (not including
# the boundary region).
xdfraa  =  53.187414
xdfdeca = -27.779152
xdfrab  =  53.159507
xdfdecb = -27.759633
xdfrac  =  53.134517
xdfdecc = -27.787144
xdfrad  =  53.161906
xdfdecd = -27.807208

xdfpolygon = --wcsmode --polygon=$(xdfraa),$(xdfdeca):$(xdfrab),$(xdfdecb):$(xdfrac),$(xdfdecc):$(xdfrad),$(xdfdecd)
