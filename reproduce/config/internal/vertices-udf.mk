# UDF region
# ==========
#
# This is the outer boundaries of the UDF region.

udfraa  =  53.164378
udfdeca = -27.835015
udfrab  =  53.113940
udfdecb = -27.792366
udfrac  =  53.161102
udfdecc = -27.748101
udfrad  =  53.211670
udfdecd = -27.789811

# The polygon option for ImageCrop used for these vertices.
udfpolygon = --polygon=$(udfraa),$(udfdeca):$(udfrab),$(udfdecb):$(udfrac),$(udfdecc):$(udfrad),$(udfdecd)
