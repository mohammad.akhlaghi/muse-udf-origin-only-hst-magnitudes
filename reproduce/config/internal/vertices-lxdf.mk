# Larger XDF vertices
# ===================
#
# This is a slightly larger area, which contains shallower boundary
# regions. We will be masking this area over the UDF, so the varying
# noise properties don't interfere with the detection and measurement
# of objects nearby.
lxdfraa  =  53.162129
lxdfdeca = -27.810275
lxdfrab  =  53.130682
lxdfdecb = -27.787376
lxdfrac  =  53.158913
lxdfdecc = -27.756522
lxdfrad  =  53.190904
lxdfdecd = -27.778703

# The polygon option for ImageCrop used for these vertices.
lxdfpolygon = --polygon=$(lxdfraa),$(lxdfdeca):$(lxdfrab),$(lxdfdecb):$(lxdfrac),$(lxdfdecc):$(lxdfrad),$(lxdfdecd)
