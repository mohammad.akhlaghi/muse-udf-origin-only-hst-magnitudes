# Name of input catalog.
#
# Only the first three columns of the input catalog are
# important. They should be ordered as (counting from 1):
#
#  Column 1: Your input ID (external to this pipeline)
#  Column 2: RA
#  Column 3: Dec
#
# The outputs will have a new ID starting at a value that is specified
# in `starting-id.mk'.
#
# If you have multiple catalogs that you want to merge, give multiple
# files to this variable (separated by a space character). They will
# all be concatenated.
inputcat = reproduce/input/input-positions.txt
