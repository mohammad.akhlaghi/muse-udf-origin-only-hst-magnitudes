# Function to set a random number gerator seed for each filter. This
# is needed for reproducible upper limit magnitudes.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
# Argument:
#
#   1) Filter

filter=$1

# Set the zero point magnitudes. These values are taken from the XDF
# survey's webpage:
if [ $filter = f160w ]; then
    echo 1
elif [ $filter = f140w ]; then
    echo 2
elif [ $filter = f125w ]; then
    echo 3
elif [ $filter = f105w ]; then
    echo 4
elif [ $filter = f850lp ]; then
    echo 5
elif [ $filter = f814w ]; then
    echo 6
elif [ $filter = f775w ]; then
    echo 7
elif [ $filter = f606w ]; then
    echo 8
elif [ $filter = f435w ]; then
    echo 9
elif [ $filter = f336w ]; then
    echo 10
elif [ $filter = f275w ]; then
    echo 11
elif [ $filter = f225w ]; then
    echo 12
else
    echo $filter" not recognized as a filter in genseed.sh."
    exit 1
fi
