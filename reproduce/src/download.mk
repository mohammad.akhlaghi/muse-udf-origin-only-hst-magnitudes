# Download all the necessary input images if they are not already
# present in the specified directories. Note that downloading is
# essentially a serial (not parallel) operation (you have one input
# into the network), so the recipes in this Makefile all use a file
# lock to have one download script running at every instant.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.




# Identify the downloader tool
# ----------------------------
downloader := $(shell if type curl > /dev/null; then downloader="curl -o";  \
	              else                           downloader="wget -O";  \
	              fi; echo "$$downloader";                               )





# XDF
# ---
xdfacspre = hlsp_xdf_hst_acswfc-$(pixelscale)0mas_hudf_
xdfwfc3irpre = hlsp_xdf_hst_wfc3ir-$(pixelscale)0mas_hudf_
xdfweb  = https://archive.stsci.edu/pub/hlsp/xdf
allxdf  = $(foreach f, $(xdfacsf), $(XDF)/$(xdfacspre)$(f)_v1_sci.fits       \
                                   $(XDF)/$(xdfacspre)$(f)_v1_wht.fits)      \
	  $(foreach f, $(xdfwfc3irf), $(XDF)/$(xdfwfc3irpre)$(f)_v1_sci.fits \
                                      $(XDF)/$(xdfwfc3irpre)$(f)_v1_wht.fits)
$(XDF):; mkdir $@
$(allxdf): $(XDF)/%: | $(XDF) $(locks)
	flock $(locks)/download -c "$(downloader) $@ $(xdfweb)/$*"





# UVUDF
# -----
segweb = https://asd.gsfc.nasa.gov/UVUDF/
r15cat = $(UVUDF)/hlsp_uvudf_hst_v2.0_cat.fits
uvudfweb = https://archive.stsci.edu/pub/hlsp/uvudf/v2.0
r15segraw = $(UVUDF)/segmentation_map_rafelski_2015.fits.gz
uvudfpre = hlsp_uvudf_hst_wfc3-uvis-$(pixelscale)0mas_udf-epoch3_
alluvudf = $(foreach f, $(udfwfc3uvf), $(UVUDF)/$(uvudfpre)$(f)_v2.0_drz.fits)
alluvudfw = $(foreach f, $(udfwfc3uvf), $(UVUDF)/$(uvudfpre)$(f)_v2.0_wht.fits)
$(UVUDF):; mkdir $@
$(r15cat) $(r15segraw): $(UVUDF)/%: | $(UVUDF)
	if [ $@ == $(r15cat) ]; then web=$(uvudfweb); else web=$(segweb); fi; \
	flock $(locks)/download -c "$(downloader) $@ $$web/$*"
$(alluvudf) $(alluvudfw): $(UVUDF)/%: | $(UVUDF) $(locks)
	flock $(locks)/download -c "$(downloader) $@ $(uvudfweb)/$*"





# For Tex:
$(mtexdir)/download.tex: reproduce/src/download.mk | $(mtexdir)
	@echo "\\newcommand{\\xdfweb}{$(xdfweb)}"             > $@
	@echo "\\newcommand{\\uvudfweb}{$(uvudfweb)}"        >> $@
	@echo "\\newcommand{\\pixelscale}{0.0$(pixelscale)}" >> $@
