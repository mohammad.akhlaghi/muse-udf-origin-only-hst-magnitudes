# Top level preparations for all the other Makefiles.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.



# Necessary high-level directories
# --------------------------------
#
# If the value to BSYM (set in `config.mk') is not blank, then build a
# symbolic link to the build directory. Note that when the value is
# empty, it is not seen as a prerequisite to `all' either.
$(BDIR):; mkdir $@;
ifneq ($(BSYM),)
$(BSYM): | $(BDIR); ln -s $(BDIR) $(BSYM)
endif
$(locks): | $(BDIR); mkdir $@





# Main initial directories
# ------------------------
#
# These are directories that are needed by many unrelated rules. When
# a directory is only needed by only a few (contextually related)
# rules, then it is defined and built around there.
fdir = $(BDIR)/fits
texdir = $(BDIR)/tex
locks = $(BDIR)/locks
catdir = $(BDIR)/catalogs
mtexdir = $(texdir)/macros
segdir = $(fdir)/segmentation
sdepth = $(fdir)/single-depth
iconf = reproduce/config/internal/

reproduce/output:; mkdir $@
$(mtexdir): | $(texdir); mkdir $@
$(segdir) $(sdepth): | $(fdir); mkdir $@
$(catdir) $(fdir) $(texdir) $(locks): | $(BDIR); mkdir $@





# Version information
# -------------------
#
# Write all the necessary information into the TeX file. The versions
# need to be updated whenever the Git file has been updated (for
# example after a commit).
gitdescribe := $(shell git describe --dirty --always --abbrev=4)
gaversion := $(shell astmkprof --version | awk 'NR==1{print $$NF}')
$(mtexdir)/versions.tex: | $(mtexdir)
	@echo "\\newcommand{\\commit}{$(gitdescribe)}"        > $@
	@echo "\\newcommand{\\gnuastrover}{$(gaversion)}"    >> $@





# Working copy of the input catalogs(s)
# -------------------------------------
cat=$(catdir)/initial-cat.txt
$(cat): $(inputcat) $(iconf)/starting-id.mk | $(catdir)

        # Note that in AWK, any undefined variable is automatically
        # initialized to 0, so the first time AWK looks at the
        # variable `c' below (before it is incremented), `c' gets a
        # value of 0. When its used the first time (after
        # incrementation), it gets a value of 1 and so on for each
        # line of the input catalog.
	awk '!/^#/{print $(startingid) + ++c, $$2, $$3, $$1}' $(inputcat) > $@
