# Make cutouts of all the objects in the catalog for inspection. Note
# that the inspection HTML is not a prerequisite of `all' by
# default. You have to explicity add it if you want to check the
# objects visually.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# We are cutting out the full catalog in this rule to select some
# sources to display in the paper. So this rule is not necessary
# needed during production, it can be optionally run to choose some
# objects. To see the final product of this step, add $(cutshtml) as a
# prerequisite to `all' in the top level `Makefile'.
r15probwwidth = 5
cutdir = $(fdir)/cutouts
alldescutdir = $(cutdir)/all-des-cutouts
$(cutdir): | $(fdir); mkdir $@
$(alldescutdir): | $(cutdir); mkdir $@





# Cutout input image
# ------------------
numincat = $(shell awk '$$1>0{++c}END{print c}' $(inputcat))
alldesfcuts = $(alldescutdir)/all-done.txt
$(alldesfcuts): $(cat) $(sdepth)/udf_$(r15probcutfilt).fits                 \
	        $(iconf)/r15prob-cutout-filter.mk .gnuastro/astimgcrop.conf \
	        | $(alldescutdir)

        # Crop them all out
	astimgcrop $(cat) $(sdepth)/udf_$(r15probcutfilt).fits              \
	           --wcsmode --wwidth=$(r15probwwidth) --racol=1            \
	           --deccol=2 --keepblankcenter --output=$(@D)              \
	           --suffix=.fits

        # say that this job is done.
	touch $@





# Cutout Rafelski+15 segmentation
# -------------------------------
alldesr15cuts = $(alldescutdir)/r15-done.txt
$(alldesr15cuts): $(cat) $(r15segraw) .gnuastro/astimgcrop.conf             \
	          | $(alldescutdir)

        # First uncompress the image because it will be really slow if
        # we use the original compressed image.
	gunzip --stdout $(r15segraw) > $@.fits

        # Crop them all out
	astimgcrop $(cat) $@.fits --wcsmode --racol=1 --deccol=2            \
	           --wwidth=$(r15probwwidth) --keepblankcenter              \
	           --output=$(@D) --suffix=_r15.fits

        # Remove the uncompressed segmentation map
	rm $@.fits

        # say that this job is done.
	touch $@





# Cutout NC's output
# -------------------------------
alldesnccuts = $(alldescutdir)/nc-done.txt
$(alldesnccuts): $(cat) $(ncmatchedseg) $(ncfdir)/udf_$(r15probcutfilt).fits \
	         .gnuastro/astimgcrop.conf | $(alldescutdir)

        # First uncompress the image because it will be really slow if
        # we use the original compressed image.
	astarithmetic $(ncfdir)/udf_$(r15probcutfilt).fits 1 gt              \
	              $(ncmatchedseg) 1 gt 4 where -h1 -h0 -o$@.fits

        # Crop them all out
	astimgcrop $(cat) $@.fits --wcsmode --racol=1 --deccol=2            \
	           --wwidth=$(r15probwwidth) --keepblankcenter              \
	           --zeroisnotblank --output=$(@D) --suffix=_nc.fits

        # Remove the uncompressed segmentation map
	rm $@.fits

        # say that this job is done.
	touch $@





# Convert to JPEG
# ---------------
dispdrange = --fluxlow=-0.02 --fluxhigh=0.005
ncdisprange = --fluxlow=0 --fluxhigh=4 --fhmaxbyte --flminbyte
catids = $(shell echo {1..$(numincat)})
alldesjcuts = $(foreach i,$(catids),$(alldescutdir)/$(i).jpg)
$(alldesjcuts): %.jpg: $(alldesfcuts) .gnuastro/astconvertt.conf
	astconvertt $*.fits $(dispdrange) -o$@

# Since the dynamic range between the labels (even in a small crop are
# very large, the labels are practically useless in the JPEG image, so
# we are just displaying everyting as black.
alldesjr15segcuts = $(foreach i,$(catids), $(alldescutdir)/$(i)_r15.jpg)
$(alldesjr15segcuts): %.jpg: $(alldesr15cuts) $(alldesnccuts)                 \
	           .gnuastro/astconvertt.conf
	astconvertt --fluxhigh=1 $*.fits -o$@

alldesjncsegcuts = $(foreach i,$(catids), $(alldescutdir)/$(i)_nc.jpg)
$(alldesjncsegcuts): %.jpg: $(alldesr15cuts) $(alldesnccuts)                 \
	           .gnuastro/astconvertt.conf
	astconvertt $(ncdisprange) $*.fits -o$@





# Make HTML for easy viewing
# --------------------------
cutshtml = $(cutdir)/check-all.html
$(cutshtml): $(alldesjcuts) $(alldesjr15segcuts) $(alldesjncsegcuts)

	echo "<html>"                                                   > $@
	echo "<style>img {width: 300; vertical-align: middle}</style>" >> $@
	echo "<body>"                                                  >> $@
	for i in $(catids); do                                                 \
	  echo '<p>'$$(($(startingid)+$$i))                            >> $@;  \
	  echo "<img src=\"$(alldescutdir)/$$i.jpg\">"                 >> $@;  \
	  echo "<img src=\"$(alldescutdir)/$$i""_r15.jpg\">"           >> $@;  \
	  echo "<img src=\"$(alldescutdir)/$$i""_nc.jpg\">"            >> $@;  \
	  echo "</p>"                                                  >> $@;  \
	done
	echo "</body></html>"                                          >> $@





# Group similar objects
# ---------------------
#
# As a final check for the classification and also to find some good
# demonstrative cases to include in the report, all the objects that
# were classified are now shown under each other.
classifiedhtml = $(cutdir)/classified.html
$(classifiedhtml): reproduce/config/manual/r15prob_classification.txt       \
	     $(alldesjcuts) $(alldesjr15segcuts) $(alldesjncsegcuts)

        # First put similarly classified objects into separate files
	awk '!/^#/{                                                         \
	       if($$2=="RD") print $$1 > "$(cutdir)/class-rd.txt";          \
	       if($$2=="RC") print $$1 > "$(cutdir)/class-rc.txt";          \
	       if($$2=="RS") print $$1 > "$(cutdir)/class-rs.txt";          \
	       if($$2=="NC") print $$1 > "$(cutdir)/class-nc.txt";          \
	       if($$2=="NS") print $$1 > "$(cutdir)/class-ns.txt";          \
	       if($$2=="F")  print $$1 > "$(cutdir)/class-f.txt";           \
	     }' reproduce/config/manual/r15prob_classification.txt

        # Now put them in the HTML, first prepare the basic HTML structure
	echo "<html>"                                                   > $@
	echo "<style>img {width: 300; vertical-align: middle}</style>" >> $@
	echo "<body>"                                                  >> $@

        # Go through all the classifications and add them in
	for class in "rd" "rc" "rs" "nc" "ns" "f"; do                         \
	  echo "<h2>$$class</h2>"                                      >> $@; \
	  while read i; do                                                    \
	    echo '<p>'$$(($(startingid)+$$i))                          >> $@; \
	    echo "<img src=\"$(alldescutdir)/$$i.jpg\">"               >> $@; \
	    echo "<img src=\"$(alldescutdir)/$$i""_r15.jpg\">"         >> $@; \
	    echo "<img src=\"$(alldescutdir)/$$i""_nc.jpg\">"          >> $@; \
	  echo "</p>"                                                  >> $@; \
	  done < $(cutdir)/class-$$class.txt;                                 \
	  echo "<p>&nbsp;</p>"                                         >> $@; \
	done

        # Close the HTML file.
	echo "</body>"                                                 >> $@
	echo "</html>"                                                 >> $@

        # Clean up
	rm $(cutdir)/class-*.txt







# Cutouts for r15prob.tex
# -----------------------
r15probdir = $(texdir)/r15prob
r15probrd = 19  26
r15probrc = 1   93
r15probrs = 29  73
r15probnc = 4   143
r15probns = 151 160
r15probf  = 72  116
r15prob = $(foreach i, $(r15probrd) $(r15probrc) $(r15probrs) $(r15probnc)  \
	      $(r15probns) $(r15probf), $(r15probdir)/$(i).pdf)

$(r15probdir): | $(texdir); mkdir $@

$(r15prob): $(r15probdir)/%.pdf: $(alldesfcuts) $(alldesr15cuts)            \
	    $(alldesnccuts) | $(r15probdir)
	astconvertt $(alldescutdir)/$*.fits $(dispdrange) --fhmaxbyte -o$@
	astconvertt $(alldescutdir)/$*_r15.fits --fluxhigh=1                \
	            -o$(r15probdir)/$*_r15.pdf
	astconvertt $(ncdisprange) $(alldescutdir)/$*_nc.fits               \
	            -o$(r15probdir)/$*_nc.pdf





# Fractions of the classes
# ------------------------
r15probfrac = $(r15probdir)/fracs.txt
$(r15probfrac): reproduce/config/manual/r15prob_classification.txt $(cat)   \
	        | $(r15probdir)

        # For a sanity check: if the number of rows are the same as
        # the input catalog then abort.
	r15numrows=$$(awk '!/^#/{++nr}END{print nr}' $<);                   \
	if [ $$r15numrows -ne $(numincat) ]; then                           \
	  echo "The number of rows between the classification";             \
	  echo "($$r15numrows) and input catalog ($(numincat))";            \
	  echo "do not have the same number of rows.";                      \
	  echo "File: reproduce/config/manual/r15prob_classification.txt";  \
	  exit 1;                                                           \
	fi

        # Now that we know they are the same number do the classification:
	awk '!/^#/{                                                         \
	       ++nr;                                                        \
	       if($$2=="RD")       ++rd;                                    \
	       else if($$2=="RC")  ++rc;                                    \
	       else if($$2=="RS")  ++rs;                                    \
	       else if($$2=="NC")  ++nc;                                    \
	       else if($$2=="NS")  ++ns;                                    \
	       else if($$2=="F")   ++f;                                     \
	       else if($$2=="O")   ++o;                                     \
	       else {                                                       \
                      print "unrecognized category " $$2 > "/dev/stderr";   \
                      exit 1;                                               \
                    }                                                       \
	     }                                                              \
	     END{ print rd/nr, rc/nr, rs/nr, nc/nr, ns/nr, f/nr, o }' $< > $@





# TeX macros
# ----------
#
# Note that the $(r15prob) PDFs are not an actual dependency of these
# macros, they are set as a prerequisite so the PDFs are built for the
# paper.
$(mtexdir)/r15prob.tex: $(cutshtml) $(r15prob) $(r15probfrac)                \
	                $(iconf)/r15prob-cutout-filter.mk

	echo "\\newcommand{\\rafprobimgbb}{\uppercase{$(r15probcutfilt)}}" > $@
	echo "\\newcommand{\\rafprobwwidth}{$(r15probwwidth)}"     >> $@

        # Put in all the classification fractions
	rd=$$(awk '{printf "%.2f", $$1*100}' $(r15probfrac));                \
	echo "\\newcommand{\\rafprobrd}{$$rd}"                     >> $@
	rc=$$(awk '{printf "%.2f", $$2*100}' $(r15probfrac));                \
	echo "\\newcommand{\\rafprobrc}{$$rc}"                     >> $@
	rs=$$(awk '{printf "%.2f", $$3*100}' $(r15probfrac));                \
	echo "\\newcommand{\\rafprobrs}{$$rs}"                     >> $@
	nc=$$(awk '{printf "%.2f", $$4*100}' $(r15probfrac));                \
	echo "\\newcommand{\\rafprobnc}{$$nc}"                     >> $@
	ns=$$(awk '{printf "%.2f", $$5*100}' $(r15probfrac));                \
	echo "\\newcommand{\\rafprobns}{$$ns}"                     >> $@
	f=$$(awk '{printf "%.2f", $$6*100}' $(r15probfrac));                 \
	echo "\\newcommand{\\rafprobf}{$$f}"                       >> $@
	nnc=$$(awk '{printf "%.2f", ($$6+$$5)*100}' $(r15probfrac));         \
	echo "\\newcommand{\\rafprobnotinnc}{$$nnc}"               >> $@
	o=$$(awk '{printf "%d", $$7}' $(r15probfrac));                       \
	echo "\\newcommand{\\rafproboutside}{$$o}"                 >> $@
	nr=$$(wc -l $(catdir)/initial-cat.txt | awk '{print $$1}');          \
	tr=$$(awk '{printf "%d", ($$1+$$2+$$3)*'$$nr'}' $(r15probfrac));     \
	echo "\\newcommand{\\rafprobinseg}{$$tr}"                  >> $@;    \
	x=$$(awk '{printf "%.2f", $$1*'$$nr'/'$$tr'*100}' $(r15probfrac));   \
	echo "\\newcommand{\\rafprobrdinrseg}{$$x}"                >> $@;    \
	x=$$(awk '{printf "%.2f", $$2*'$$nr'/'$$tr'*100}' $(r15probfrac));   \
	echo "\\newcommand{\\rafprobrcinrseg}{$$x}"                >> $@;    \
	x=$$(awk '{printf "%.2f", $$3*'$$nr'/'$$tr'*100}' $(r15probfrac));   \
	echo "\\newcommand{\\rafprobrsinrseg}{$$x}"                >> $@;    \
