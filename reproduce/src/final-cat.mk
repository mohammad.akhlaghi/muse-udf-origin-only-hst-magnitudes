# The final segmentation map has been built, here we will use it to
# make a catalog in each broadband filter and then merge them all
# together to make one final catalog.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.



# Raw catalog for the flat fields
# -------------------------------
scatdir = $(catdir)/separate-out
scatsdepth = $(foreach f, $(allfilters), $(scatdir)/udf_$(f)_raw.txt)      \
             $(foreach f, $(xdfwfc3irf), $(scatdir)/xdf_$(f)_raw.txt)
$(scatdir): | $(catdir); mkdir $@
$(scatsdepth): $(scatdir)/%_raw.txt: $(ncfdir)/%.fits $(ncandaper)         \
	       reproduce/src/genseed.sh reproduce/src/zeropoints.sh        \
	       $(iconf)/rng-type.mk .gnuastro/astmkcatalog.conf | $(scatdir)

        # Make the catalog for this filter:
	filter=$(word 2,$(subst _, ,$*));                                  \
	zp=$$(reproduce/src/zeropoints.sh $$filter);                       \
	export GSL_RNG_TYPE=$(rngtype);                                    \
	export GSL_RNG_SEED=$$(reproduce/src/genseed.sh $$filter);         \
	astmkcatalog --zeropoint=$$zp $< --objlabs=$(ncandaper) --objhdu=0 \
	             --upmask=$< --upmaskhdu=1 --upperlimitmag --envseed   \
	             --numthreads=1 -o$@





# Raw catalog for the gradient region
# -----------------------------------
#
# Note that we cannot simply mask the two flat regions, because the
# noise varies strongly over the gradient region and detection is not
# useful. On the other hand, the segments might partly lie over the
# deep or wide regions.
scatgrad = $(foreach f, $(xdfwfc3irf), $(scatdir)/grd_$(f)_raw.txt)
$(scatgrad): $(scatdir)/grd_%_raw.txt: $(ncfdir)/udf_%.fits                \
	     $(sdepth)/grd_%.fits $(ncandaper) reproduce/src/genseed.sh    \
	     reproduce/src/zeropoints.sh .gnuastro/astarithmetic.conf      \
	     .gnuastro/astmkcatalog.conf | $(scatdir)

        # Generate the mask for this filter. Note that we only want a
        # small number of chosen objects, so unlike the case for a
        # flat region (where we could use the input and mask image
        # that corresponded to each other) here we don't have any
        # detections over the full multi-depth image. We will be using
        # the noise properties over the wide region for detections
        # over the gradient region.
	astarithmetic $< isblank $(ncfdir)/udf_$*.fits 0 gt or -h0 -h1     \
	              -o$(scatdir)/grd_$*_up.fits.gz --type=long

        # Make the catalog.
	zp=$$(reproduce/src/zeropoints.sh $*);                             \
	export GSL_RNG_TYPE=$(rngtype);                                    \
	export GSL_RNG_SEED=$$(reproduce/src/genseed.sh $*);               \
	astmkcatalog --zeropoint=$$zp $(sdepth)/grd_$*.fits                \
	             --objlabs=$(ncandaper)                --objhdu=0      \
	             --skyfilename=$(ncfdir)/udf_$*.fits   --skyhdu=3      \
	             --stdfilename=$(ncfdir)/udf_$*.fits   --stdhdu=4      \
	             --upmask=$(scatdir)/grd_$*_up.fits.gz --upmaskhdu=0   \
	             --upperlimitmag --envseed  --numthreads=1 -o$@

        # Clean up
	rm $(scatdir)/grd_$*_up.fits.gz





# Processed catalog for all cases
# ---------------------------------
#
# The raw catalogs are processed by removing the extra rows and
# columns while also using the upper-limit magnitude when necessary.
scat = $(subst _raw.txt,.txt,$(scatsdepth) $(scatgrad))
$(scat): $(scatdir)/%.txt: $(scatdir)/%_raw.txt

        # Only keep the rows and columns that we want and replace the
        # upper limit magnitude when necessary.
	awk '!/^#/ && $$1>$(startingid){                                   \
	       if( $$4=="nan" && $$5=="nan" )                              \
	         { mag=-1; magerr=-1; }                                    \
	       else if( $$4>$$7 || ($$4=="nan" && $$5!="nan") )            \
	         { mag=$$7; magerr=-1; }                                   \
	       else                                                        \
	         { mag=$$4; magerr=$$5; }                                  \
	       print $$1, $$2, $$3, mag, magerr, $$6}' $< > $@





# Set the IR depth
# ----------------
irdepth = $(catdir)/ir-depth.txt
$(irdepth): $(scatdir)/xdf_f160w.txt $(scatdir)/udf_f160w.txt
	awk '$$4>0 || $$5>0 {status[FNR]=NR-FNR}                           \
	     END{for(i=1;i<=FNR;++i) {                                     \
	           if(status[i]=="") print "gradir";                       \
	           else print status[i]==FNR ? "wideir" : "deepir";        \
	        }}' $^ > $@





# Unified catalog for the IR filters
# ----------------------------------
mergeddir = $(catdir)/merged-ir
irmerged = $(foreach f, $(xdfwfc3irf), $(mergeddir)/udf_$(f).txt)
$(mergeddir): | $(catdir); mkdir $@
$(irmerged): $(mergeddir)/udf_%.txt: $(scatdir)/xdf_%.txt                  \
	     $(scatdir)/grd_%.txt $(scatdir)/udf_%.txt | $(mergeddir)

        # First merge the flat field catalogs. Note that there can be
        # no overlap between the deep and shallow fields. So we can
        # simply store each row from the catalog that contains it.
	awk '$$4>0 || $$5>0 { out[FNR]=$$0 }                               \
	     END{ for(i=1;i<=FNR;++i)                                      \
	            print out[i]=="" ? "grad" : out[i];                    \
	        }' $(scatdir)/xdf_$*.txt $(scatdir)/udf_$*.txt > $@.t

        # Now put in the gradient's magnitude.
	awk 'NR==FNR && $$1!="grad"  { out[FNR]=$$0 }                      \
	     NR!=FNR && out[FNR]=="" { out[FNR]=$$0 }                      \
	     END { for(i=1;i<=FNR;++i) print out[i]; }'                    \
	    $@.t $(scatdir)/grd_$*.txt > $@

        # Clean up
	rm $@.t





# Unified catalogs for all filters
# --------------------------------
#
# Because of the varying depth of the IR region, we made multiple
# catalogs for the IR and merged them above. Here we are putting them
# all under a fixed naming convention so we can easily make the final
# catalog.
unifieddir=$(catdir)/unified
unifieduvopt = $(foreach f, $(udfwfc3uvf) $(xdfacsf),                      \
	          $(unifieddir)/udf_$(f).txt)
unifiedir = $(foreach f, $(xdfwfc3irf), $(unifieddir)/udf_$(f).txt)
$(unifieddir): | $(catdir); mkdir $@
$(unifieduvopt): $(unifieddir)/%: $(scatdir)/% | $(unifieddir)
	cp $< $@
$(unifiedir): $(unifieddir)/%: $(mergeddir)/% | $(unifieddir)
	cp $< $@





# Final catalog along with coordinates of apertures
# -------------------------------------------------
#
# Make the final catalog, and as a side effect (not an explicit target
# of this rule) select the coordinates of the objects where we have
# used aperture photomery, so we can make them over the NoiseChisel
# segmentation map for the final result.
finalcat=reproduce/output/hst-raf-comp-cat.txt
$(finalcat): $(unifieduvopt) $(unifiedir) $(irdepth) $(bestfilter)    \
	     $(iconf)/filter-for-position.mk | reproduce/output


        # Print the information of each column.
	date=$$(date);                                                    \
	echo "# Generated from commit $(gitdescribe)"           > $@.top; \
	echo "# Using Gnuastro $(gaversion)"                   >> $@.top; \
	echo "# At $$date"                                     >> $@.top
	echo "# Column 1: new_raf_id [label, u16] ID based on Rafelski+15 method (starting from 60000)" >> $@.top
	echo "# Column 2: ra [degrees, f64] Right ascension based on $(positionfilter) filter image" >> $@.top
	echo "# Column 3: dec [degrees, f64] Declination based on $(positionfilter) filter image" >> $@.top
	echo "# Column 4: area [count, u16] Number of pixels occupied by segmentation map" >> $@.top
	echo "# Column 5: muse_udf_id [label, u16] MUSE UDF catalog ID" >> $@.top
	apradarcsec=$$(echo " " | awk                                     \
	               '{printf "%.2f", $(apertureradius)*0.06}');        \
	echo "# Column 6: seg_field [name, str5] Seg IR field (aper: artificial $$apradarcsec arcsec radius aperture)" >> $@.top
	echo "# Column 7: seg_filter [name, str6, nan] Filter which segmentation map was derived from."                >> $@.top
	echo "# Column 8: ir_depth [name, str6] If object was in the deep or wide IR fields." >> $@.top
	counter=9;                                                        \
	for filter in $(allfilters); do                                   \
	  echo "# Column $$counter: mag_$$filter [AB magnitude, f32, -1] AB magnitude in $$filter filter"              >> $@.top;\
	  ((counter++));                                                  \
	  echo "# Column $$counter: mag_err_$$filter [AB magnitude, f32, -1] Error in measuring $$filter magnitude"    >> $@.top;\
	  ((counter++));                                                  \
	done


        # Add the first few columns (position from the F775W filter):
	awk '{printf "%-7d%-13.8f%-14.8f%-5d\n", $$1, $$2, $$3, $$6}'    \
	    $(unifieddir)/udf_$(positionfilter).txt > $@.coords
	awk '{printf "%-6d%-6s%-7s\n", $$4, $$5=="nan" ? "aper" : $$5,   \
	                               $$6}' $(bestfilter) > $@.info
	awk '{printf "%-7s\n", $$1}' $(irdepth) > $@.fg
	paste -d' ' $@.coords $@.info > $@.fc
	paste -d' ' $@.fc $@.fg > $@.cols
	rm $@.coords $@.info $@.fg $@.fc


        # Add the magnitudes for each filter. To do it cleanly, we
        # will first merge the depth and upper-limit tables for this
        # filter.
	for filter in $(allfilters); do                                  \
	  awk '{printf "%-7.3f %-8.3f\n", $$4, $$5}'                     \
	      $(unifieddir)/udf_$$filter".txt" > $@.t;                   \
	  paste -d' ' $@.cols $@.t > $@.m;                               \
	  mv $@.m $@.cols;                                               \
	  rm $@.t;                                                       \
	done


        # Add the column information to the catalog and delete the
        # extra files.
	cat $@.top $@.cols > $@
	rm $@.top $@.cols





# Cutout selected objects for paper
# ---------------------------------
#
# Generate cutouts of the selected objects to include in the final
# paper.
demo-obj = 6524
demo-cutouts = reproduce/output/$(demo-obj)-demo.tar.gz
$(demo-cutouts): $(finalcat) $(finalseg)

        # Cutout all the necessary images.
	infos=$$(awk '$$5==$(demo-obj){                                       \
                printf "--ra=%s --dec=%s %s", $$2, $$3, $$7}' $(finalcat));   \
	cutops=$$(echo $$infos | awk '{print $$1, $$2}');                     \
	filter=$$(echo $$infos | awk '{print $$3}');                          \
	astimgcrop $$cutops --wwidth=8 $(r15seg) -o$(@D)/demo-r15seg.fits;    \
	astimgcrop $$cutops --wwidth=8 $(finalseg) -o$(@D)/demo-newseg.fits;  \
	astimgcrop $$cutops --wwidth=8 $(ncfdir)/udf_$$filter.fits            \
                   -o$(@D)/demo-filter.fits;                                  \
	astimgcrop $$cutops --wwidth=8 $(ncfdir)/udf_$$filter.fits -h2        \
                   -o$(@D)/demo-clumps.fits;

        # Convert the 1 values to 4 and the -2 values to 1 for easy
        # plotting.
	img=$(@D)/demo-clumps.fits;                                           \
	astarithmetic $$img $$img 1 eq 4 where $$img -2 eq 1 where            \
	              -o$(@D)/demo-clumps-corr.fits;
	mv $(@D)/demo-clumps-corr.fits $(@D)/demo-clumps.fits

        # Put them in a tarball
	cd $(@D); tar -zcf $(notdir $@) demo-r15seg.fits demo-newseg.fits     \
	                   demo-filter.fits demo-clumps.fits

        # Clean up
	rm $(@D)/demo-r15seg.fits $(@D)/demo-newseg.fits                      \
	   $(@D)/demo-filter.fits $(@D)/demo-clumps.fits



# Write necessary information in LaTeX
# ------------------------------------
$(mtexdir)/final-cat.tex: reproduce/config/gnuastro/astmkcatalog.conf      \
	                 $(finalcat)

        # The number of objects on the gradient region. The gradient
        # row is on column 8. Note that we also need to cound the
        # number of uncommented rows, we cannot simply use AWK's NR
        # variable.
	ng=$$(awk '!/^#/ {++nr}                                            \
	           $$8=="gradir" {++ng}                                    \
	           END{printf "%.1f", ng/nr*100}' $(finalcat));            \
	echo "\\newcommand{\\fracovergradir}{$$ng}"                 > $@

        # The number of randomly placed objects for each upper limit
        # measurement.
	out=$$(awk '/upnum/{print $$2}'                                    \
	           reproduce/config/gnuastro/astmkcatalog.conf);           \
	echo "\\newcommand{\\mkcatupnum}{$$out}"                   >> $@

        # Multiple of sigma for upper limit magnitude
	for k in upnsigma upsclipmultip upsclipaccu; do                    \
	  out=$$(awk '/'$$k'/{print $$2}'                                  \
	             reproduce/config/gnuastro/astmkcatalog.conf);         \
	  echo "\\newcommand{\\mkcat$$k}{$$out}"                   >> $@;  \
	done
