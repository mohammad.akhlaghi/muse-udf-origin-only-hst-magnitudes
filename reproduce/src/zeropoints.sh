# zeropoints.sh: Function to find the zeropoints in each filter
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.
#
# Argument:
#
#   1) Filter

filter=$1

# Set the zero point magnitudes. These values are taken from the XDF
# survey's webpage:
if [ $filter = f160w ]; then
    echo 25.94
elif [ $filter = f140w ]; then
    echo 26.45
elif [ $filter = f125w ]; then
    echo 26.23
elif [ $filter = f105w ]; then
    echo 26.27
elif [ $filter = f850lp ]; then
    echo 24.87
elif [ $filter = f814w ]; then
    echo 25.94
elif [ $filter = f775w ]; then
    echo 25.69
elif [ $filter = f606w ]; then
    echo 26.51
elif [ $filter = f435w ]; then
    echo 25.68
elif [ $filter = f336w ]; then
    echo 24.67
elif [ $filter = f275w ]; then
    echo 24.13
elif [ $filter = f225w ]; then
    echo 24.04
else
    echo $filter" not recognized as a filter for the zeropoint magnitude."
    exit 1
fi
