# Find the magnitudes and S/N of objects using apertures and the raw
# weight images from the HST webpage. This can be used to show the
# faint-ness of the objects, irrespective of the detection method
# used.
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.



# Correction factors
# ------------------
#
# The raw `wht' maps that are derived from the pipeline are actually
# exposure maps. Assuming no pixel mixing, exposure maps can be
# converted to a standard deviation image (with units of `count/sec')
# from this relation: `1/sqrt(exposure)'.
#
# However, when pixels will be mixed (`pixfrac>0' in the HST
# pipeline), we need to calculate a correction factor. For a nice
# discussion, see Section 7 ("Noise in drizzled images") of Fruchter
# and Hook 2002 (PASP 114, 144). As a summary, assuming the output
# scale is `s' and `pixfrac' is `p' (both in units of input pixels),
# we can define `r=p/s', then the correction factor (R) is:
#
#           r>=1   --->      R = r / ( 1 - (1 / (3*r) ) )
#           r<=1   --->      R = 1 / ( 1 - (r / 3     ) )
#
# The corresponding values for the datasets we are using are (from
# their respective papers), the UVUDF pixfrac was taken from the
# `D001PIXF' FITS keyword, it isn't written in the paper:
#
#                    p    instrument    s          r          R
#                          pixscale
#                  -----------------------------------------------
#   UVUDF/WFC3-UV   0.8      0.04      1.5        0.53       1.22
#   XDF/ACS          0       0.05      1.2         0          1
#   XDF/WFC3-IR     0.8      0.13      0.5        1.60       2.02
#
# The raw standard deviation (derived from the exposure maps) should
# be divided by R to give the proper standard deviation.
R-uvudf-wfc3uv = 1.22
R-xdf-acs      = 1
R-xdf-wfc3ir   = 2.02





# Correct the exposure maps
# -------------------------
#
# As discussed above, convert the raw exposure maps into standard
# deviation images.
stdfexpdir = $(fdir)/std-from-exp
$(stdfexpdir): | $(fdir); mkdir $@
std-from-exp-uvudf   = $(foreach f, $(udfwfc3uvf), $(stdfexpdir)/udf_$(f).fits)
$(std-from-exp-uvudf): $(stdfexpdir)/udf_%.fits:                            \
                       $(UVUDF)/$(uvudfpre)%_v2.0_wht.fits | $(stdfexpdir)
	astimgcrop $(udfpolygon) --hstartwcs=1 --hendwcs=40 $<              \
                   -o$(basename $@)_crop.fits
	astarithmetic 1 $(basename $@)_crop.fits sqrt /                     \
                   $(R-uvudf-wfc3uv) / -o$@
	rm $(basename $@)_crop.fits

std-from-exp-xdfacs  = $(foreach f, $(xdfacsf),    $(stdfexpdir)/udf_$(f).fits)
$(std-from-exp-xdfacs): $(stdfexpdir)/udf_%.fits:                           \
                        $(XDF)/$(xdfacspre)%_v1_wht.fits | $(stdfexpdir)
	astimgcrop $(udfpolygon) --hstartwcs=1 --hendwcs=40 $<              \
                   -o$(basename $@)_crop.fits
	astarithmetic 1 $(basename $@)_crop.fits sqrt /                     \
                   $(R-xdf-acs) / -o$@
	rm $(basename $@)_crop.fits

std-from-exp-xdfwfc3 = $(foreach f, $(xdfwfc3irf), $(stdfexpdir)/grd_$(f).fits)
$(std-from-exp-xdfwfc3): $(stdfexpdir)/grd_%.fits:                          \
                        $(XDF)/$(xdfwfc3irpre)%_v1_wht.fits | $(stdfexpdir)
	astimgcrop $(udfpolygon) --hstartwcs=1 --hendwcs=40 $<              \
                   -o$(basename $@)_crop.fits
	astarithmetic 1 $(basename $@)_crop.fits sqrt /                     \
                   $(R-xdf-wfc3ir) / -o$@
	rm $(basename $@)_crop.fits





# Make the aperture catalog
# -------------------------
#
# We want to make an aperture of diameter 0.4 arcseconds on all the
# input objects.
whtapercat = $(catdir)/apertures-wht-cat.txt
$(whtapercat): $(cat)
	awk '{print($$1, $$2, $$3, 4, $(aperturewhtradius), 1, 0, 1, $$1, 1)}'\
	    $< > $@




# Make the segmentation map
# -------------------------
#
# A segmentation map is necessary to generate the magnitudes and their
# error. We will use one of the images as a canvas for the profiles.
whtaperseg = $(segdir)/wht-apertures.fits.gz
$(whtaperseg): $(whtapercat) $(sdepth)/udf_f775w.fits
	astmkprof $(whtapercat) $(sdepth)/udf_f775w.fits --mforflatpix      \
                  --oversample=1 --racol=1 --deccol=2 --inputascanvas       \
                  --type=long -o$@





# Make the individual filter catalogs
# -----------------------------------
#
# With all the separate components ready, we can now build the raw
# catalogs. For the filters that we have
whtapercatdir = $(catdir)/wht-apers
$(whtapercatdir): | $(catdir); mkdir $@

filtwhtaper = $(foreach f, $(udfwfc3uvf) $(xdfacsf),                        \
                           $(whtapercatdir)/udf_$(f).txt)                   \
              $(foreach f, $(xdfwfc3irf), $(whtapercatdir)/grd_$(f).txt)
$(filtwhtaper): $(whtapercatdir)/%.txt: $(sdepth)/%.fits $(ncfdir)/%.fits   \
                $(whtaperseg) $(stdfexpdir)/%.fits                          \
                .gnuastro/astmkcatalog.conf | $(whtapercatdir)

        # Note that the IR images don't have a NoiseChisel detection
        # in this mode because they have a gradient.
	filter=$(word 2,$(subst _, ,$*));                                   \
	zp=$$(reproduce/src/zeropoints.sh $$filter);                        \
	astmkcatalog --zeropoint=$$zp $< --numthreads=1 -o$@                \
                     --objlabs=$(whtaperseg)              --objhdu=0        \
                     --skyfilename=$(ncfdir)/$*.fits      --skyhdu=3        \
                     --stdfilename=$(stdfexpdir)/$*.fits  --stdhdu=0        \
                     --sn --magnitude




# Merge all the catalogs into one
# -------------------------------
#
# Merge the catalogs from all the filters into the final catalog
final-wht-cat = reproduce/output/hst-raf-comp-aper-cat.txt
$(final-wht-cat): $(filtwhtaper) | reproduce/output

        # Just to make sure the users don't confuse this with the main
        # catalog.
	echo "# CATALOG OF PURE APERTURES WITH NO DETECTION"    > $@.top
	echo "# "                                              >> $@.top

        # Print the information of each column.
	date=$$(date);                                                    \
	echo "# Generated from commit $(gitdescribe)"          >> $@.top; \
	echo "# Using Gnuastro $(gaversion)"                   >> $@.top; \
	echo "# At $$date"                                     >> $@.top
	echo "# Column 1: new_raf_id [label, u16] ID based on Rafelski+15 method (starting from 60000)" >> $@.top
	echo "# Column 2: ra [degrees, f64] Right ascension based on $(positionfilter) filter image" >> $@.top
	echo "# Column 3: dec [degrees, f64] Declination based on $(positionfilter) filter image" >> $@.top
	counter=4;                                                        \
	for filter in $(allfilters); do                                   \
	  echo "# Column $$counter: mag_$$filter [AB magnitude, f32, -1] AB magnitude in $$filter filter"              >> $@.top;                         \
	  ((counter++));                                                  \
	  echo "# Column $$counter: sn_$$filter [ratio, f32, -1] S/N in $$filter magnitude"    >> $@.top;\
	  ((counter++));                                                  \
	done

        # Use the respective filter for the ID and central positions
	awk '$$1>60000 {printf "%-7d%-13.8f%-14.8f\n", $$1, $$2, $$3}'    \
	    $(whtapercatdir)/udf_$(positionfilter).txt > $@.cols

        # For an easy next step, we'll make symbolic links to the IR
        # filters with the name of `udf_'.
	for filter in $(xdfwfc3irf); do                                  \
	  if [ ! -f $(whtapercatdir)/udf_$$filter.txt ]; then            \
	    ln -s $(whtapercatdir)/grd_$$filter.txt                      \
                  $(whtapercatdir)/udf_$$filter.txt;                     \
	  fi;                                                            \
	done

        # Add the magnitudes and S/N for each filter.
	for filter in $(allfilters); do                                  \
	  awk '$$1>60000 {if($$6!="nan") printf("%-7.3f %-8.3f\n", $$6, $$7); \
                          else           printf("%-7.3f %-8.3f\n", -1, -1);}' \
	      $(whtapercatdir)/udf_$$filter".txt" > $@.t;                \
	  paste -d' ' $@.cols $@.t > $@.m;                               \
	  mv $@.m $@.cols;                                               \
	  rm $@.t;                                                       \
	done

        # Add the column information to the catalog and delete the
        # extra files.
	cat $@.top $@.cols > $@
	rm $@.top $@.cols
