/*********************************************************************
Given two sorted catalogs and a maximum radius, give the row number of
the objects that match in both catalogs: objects that are nearest to
each other within the given radius. If two (or any other number) of
objects in one catalog correspond to (are within the maximum radius
of) two (or any other number) of objects in the other catalog, this
program will unrepeatedly identify the nearest ones, giving one unique
(and nearest) match.

Inputs:

  - The positional parameters ((X,Y), or (RA, Dec)) have to be on rows 1
    and 2 (counting from 0).

  - They have to be sorted by the first axis coordinate.

How to call:

    This program takes three arguments: two text files and one
    number. The number is the maximum search distance. The units of
    this number are the same as the position units in the catalogs:
    pixels, or degrees for example.

     $ ./matchcats catalog1.txt catalog2.txt 5

Output:

  - For each match, there is a row with three columns: the first
    column is the row number of an object in the first catalog and the
    second is the row number of the corresponding object in the second
    catalog (both starting from one). The third row is the distance
    between the two.

Original author:
    Mohammad Akhlaghi <mohammad@akhlaghi.org>
Contributing author(s):
Copyright (C) 2016-2018, Mohammad Akhlaghi.

This program free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.
**********************************************************************/
#include <math.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include <gsl/gsl_sort.h>
#include <gnuastro/txtarray.h>
#include <gnuastro/linkedlist.h>
#include <gsl/gsl_permute_double.h>










/**********************************************************************/
/*****************          Basic structures        *******************/
/**********************************************************************/
struct linkedlist_sfll
{
  float f;
  size_t v;
  struct linkedlist_sfll *next;
};





struct params
{
  char       *progname;	             /* Program name for error messages.   */
  double            *a;              /* First catalog (only coordinates).  */
  double            *b;              /* Second catalog (only coordinates). */
  double       maxdist;	   	     /* Maximum acceptable distance.       */
  size_t            ar;		     /* Number of rows in a.               */
  size_t            br;		     /* Number of rows in b.               */
  size_t         axcol;              /* First axis column number in a.     */
  size_t         aycol;              /* Second axis column number in a.    */
  size_t         bxcol;              /* First axis column number in b.     */
  size_t         bycol;              /* Second axis column number in b.    */
  size_t *apermutation;              /* Original indexs (before sorting).  */
  size_t *bpermutation;              /* Original indexs (before sorting).  */
  struct linkedlist_sfll **bina;     /* We don't need order here.          */
};




















/**********************************************************************/
/*****************       Linked list functions      *******************/
/**********************************************************************/
void
linkedlist_add_to_sfll(struct linkedlist_sfll **list,
		       size_t value, float fvalue)
{
  struct linkedlist_sfll *newnode;

  errno=0;
  newnode=malloc(sizeof *newnode);
  if(newnode==NULL)
    error(EXIT_FAILURE, errno,
	  "linkedlist: New element in linkedlist_sfll");

  newnode->v=value;
  newnode->f=fvalue;
  newnode->next=*list;
  *list=newnode;
}





void
linkedlist_pop_from_sfll(struct linkedlist_sfll **list,
			 size_t *value, float *fvalue)
{
  struct linkedlist_sfll *tmp;
  tmp=*list;
  *value=tmp->v;
  *fvalue=tmp->f;
  *list=tmp->next;
  free(tmp);
}


















/**********************************************************************/
/****************           Second layer            *******************/
/**********************************************************************/
/* The GNU C Library's error function is great for printing errors,
   but we might need to compile this script independently on other
   systems that don't have such a functionality. So I am defining this
   function here to simplify the code. */
void
allocerror(struct params *p, size_t size, char *name, int errnumber)
{
  fprintf (stderr, "%s: %lu bytes for %s; %s\n", p->progname,
	   size, name, errnumber ? strerror(errnumber): "");
  exit(EXIT_FAILURE);
}





/* Go through both catalogs and find which records/rows in the second
   catalog (catalog b) are within the acceptable distance of each
   record in the first (a). */
void
fillbina(struct params *p)
{
  /* To keep things easy to read, all variables related to catalog 1
     are start with an `a' and things related to catalog 2 are marked
     with a `b'. The redundant variables (those that equal a previous
     value) are only defined to make it easy to read the code.*/
  size_t ar=p->ar, br=p->br;
  size_t ai, bi, blow, prevblow=0;
  double ax, ay, bx, by, r, maxdist=p->maxdist;


  /* For each row/record of catalog a, make a list of the nearest
     records in catalog b within the maximum distance. Note that both
     catalogs are sorted by their first axis coordinate.*/
  for(ai=0;ai<ar;++ai)
    {
      /* Set the initial parameters*/
      p->bina[ai]=NULL;
      ax=p->a[ai*2  ];
      ay=p->a[ai*2+1];


      /* Find the first (lowest first axis value) row/record in the
	 catalog b that is within the search radius for this record of
	 catalog a. `blow' is the index of the first element to start
	 searching in the catalog b for a match to `sa' (the record in
	 catalog a that is currently being searched). `blow' is only
	 based on the first coordinate, not the second.

         Both catalogs are sorted by their first coordinate, so the
	 `blow' to search for the next record in catalog a will be
	 larger or equal to that of the previous catalog a record. To
	 account for possibly large distances between the records, we
	 do a search here to change `blow' if necessary before doing
	 further searching.*/
      for( blow=prevblow; blow<ar && p->b[blow*2] < ax-maxdist; ++blow)
	{/* This can be blank, the `for' does all we need :-). */}


      /* `blow' is now found for this object and will be used
	 unchanged to the end of the loop. So keep its value to help
	 the search for the next entry in catalog a. */
      prevblow=blow;


      /* Go through catalog b (starting at `blow') with a first axis
	 value smaller than the maximum acceptable range for `si'. */
      for( bi=blow; bi<br && p->b[bi*2] <= ax+maxdist;
	   ++bi )
	{
	  /* Only consider records with a second axis value in the
	     correct range, note that unlike the first axis, the
	     second axis is no longer sorted. so we have to do both
	     lower and higher limit checks for each item.

	     Positions can have an accuracy to a much higher order of
	     magnitude than the search radius. Therefore, it is
	     meaning-less to sort the second axis (after having sorted
	     the first). In other words, very rarely can two first
	     axis coordinates have EXACTLY the same floating point
	     value as each other to easily define an independent
	     sorting in the second axis. */
	  bx = p->b[bi*2  ];
	  by = p->b[bi*2+1];
	  if( by >= ay-maxdist && by <= ay+maxdist )
	    {
	      /* Now, `bi' is within the square range of `bi'. But
		 this is not enough to consider the two objects
		 matched for the following reasons:

		 1) Until now we have avoided calculations other than
		 larger or smaller on double precision floating point
		 variables for efficiency. So the `bi' is within a
		 square of side `2*maxdist' around `ai' (not a circle
		 of radius `maxdist').

		 2) Other objects in the long catalog may be closer to
		 `ai' than this `bi'.

		 3) The closest `bi' to `ai' might be closer to
		 another catalog a record.

		 To address these problems, we will be using a linked
		 list to keep the indexes of the objects near `ai',
		 along with their distance. We only add the `bi's to
		 this linked list that are within the acceptable
		 distance. Since we are dealing with much fewer
		 objects at this stage, it is justified to do complex
		 mathematical operations like square root and times.

		 So the first problem is solved. The next two problems
		 are going to be addressed after going through all the
		 relevant `bi's for this `ai'.*/
	      r = sqrt((bx-ax)*(bx-ax)+(by-ay)*(by-ay));
	      if(r<maxdist)
		linkedlist_add_to_sfll(&p->bina[ai], bi, r);
	    }
	}


      /* If there was no objects within the acceptable distance, then
	 the linked list pointer will be NULL, so go on to the next
	 `ai'. */
      if(p->bina[ai]==NULL)
	continue;

      /* For checking the status of affairs uncomment this block
      {
	struct linkedlist_sfll *tosll;
	printf("\n\nai: %lu:\n", ai);
	printf("ax: %f (%f -- %f)\n", ax, ax-maxdist, ax+maxdist);
	printf("ay: %f (%f -- %f)\n", ay, ay-maxdist, ay+maxdist);
	for(tosll=p->bina[ai];tosll!=NULL;tosll=tosll->next)
	  printf("%lu: %f\n", tosll->v, tosll->f);
      }
      */
    }
}





/* Now reverse the bina array to fill in the ainb array.  */
void
rearange(struct params *p)
{
  size_t bi, bijunk, aijunk;
  size_t ai, ar=p->ar, br=p->br;
  float *fp, *fpf, r, rjunk, *ainb;
  struct linkedlist_sfll **bina=p->bina;

  /* Allocate the space for `ainb' and initialize it to NaN (since
     zero is meaningful) in this context (both for indexs and also for
     floats). This is a two column array that will keep the distance
     and index of the closest element in catalog a for each element in
     catalog b. */
  errno=0; ainb=calloc(2*br, sizeof *ainb);
  if(ainb==NULL) allocerror(p, br*sizeof *ainb, "ainb", errno);
  fpf=(fp=ainb)+2*br; do *fp++=NAN; while(fp<fpf);

  /* Go over each object in catalog `a' and re-distribute the near
     objects, to find which ones in catalog a are within the search
     radius of catalog b in a sorted manner. Note that we only need
     the `ai' with the minimum distance to `bi', the rest are junk.*/
  for( ai=0; ai<ar; ++ai )
    while( bina[ai] )	/* As long as its not NULL.            */
      {
	/* Pop out a `bi' and its distance to this `ai' from
	   `bina'. */
	linkedlist_pop_from_sfll(&bina[ai], &bi, &r);

	/* If nothing has been put here (the isnan condition below is
	   true), or something exists (the isnan is false, and so it
	   will check the second OR test) with a distance that is
	   larger than this distance then just put this value in. */
	if( isnan(ainb[bi*2]) || r<ainb[bi*2+1] )
	  {
	    ainb[bi*2  ] = ai;
	    ainb[bi*2+1] = r;
	  }
      }

  /* For checking the status of affairs uncomment this block
  {
    printf("\n\nFilled ainb:\n");
    for(bi=0;bi<br;++bi)
      if( !isnan(ainb[bi*2]) )
	printf("bi: %lu: %.0f, %f\n", bi, ainb[bi*2], ainb[bi*2+1]);
  }
  */

  /* Re-fill the bina array, but this time only with the `bi' that is
     closest to it. Note that bina was fully set to NULL after popping
     all the elements in the loop above.*/
  for( bi=0; bi<br; ++bi )
    if( !isnan(ainb[bi*2]) )
      {
	/* Just to keep the same terminology as before and easier
	   reading.*/
	r=ainb[bi*2+1];
	ai=(size_t)(ainb[bi*2]);

	/* Check if this is the first time we are associating a `bi'
	   to this `ai'. */
	if( bina[ai] )
	  {	 /* Brace is necessary to not confuse the `else'.*/
	    /* If the distance of this record is smaller than the
	       existing entry, then replace the values. */
	    if( r < bina[ai]->f )
	      {
		bina[ai]->f=r;
		bina[ai]->v=bi;
	      }
	  }
	else
	  linkedlist_add_to_sfll(&bina[ai], bi, r);
      }

  /* For checking the status of affairs uncomment this block
  printf("Rearranged bina:\n");
  for(ai=0;ai<ar;++ai)
    if(bina[ai])
      printf("ai: %lu: %lu, %f\n", ai, bina[ai]->v, bina[ai]->f);
  */

  /* Clean up */
  free(ainb);
}



















/**********************************************************************/
/****************    First layer (called by main)   *******************/
/**********************************************************************/
void
setparams(struct params *p, int argc, char **argv)
{
  char *tailptr;
  size_t i, ac, bc, sr;
  double *fulla, *fullb, tmp;


  /* Keep the executable name for error reporting: */
  p->progname=argv[0];


  /* Check if the correct number of arguments are given. Note that the
     executable name is also counted as a token on the
     command-line. */
  if(argc!=4)
    {
      fprintf(stderr, "%s: There should be three arguments: two text "
	      "tables and a number (for the maximum acceptable distance).",
	      p->progname);
      exit(EXIT_FAILURE);
    }

  /* Read the input catalogs into the memory in temporary pointers, we
     will sort them and extract only the coordinates to keep here.*/
  gal_txtarray_txt_to_array(argv[1], &fulla, &p->ar, &ac);
  gal_txtarray_txt_to_array(argv[2], &fullb, &p->br, &bc);


  /* Read the maximum acceptable distance: */
  p->maxdist=strtod(argv[3], &tailptr);
  if(tailptr==argv[3])
    {
      fprintf(stderr, "%s: %s could not be read as a number.",
	      argv[3], p->progname);
      exit(EXIT_FAILURE);
    }


  /* Set the first and second axis column numbers. Note that currently
     these are fixed, but it is easy to make them separate for each
     catalog later. */
  p->axcol=p->bxcol=1;
  p->aycol=p->bycol=2;


  /* Allocate space for the coordinate arrays we want to work with
     (only the first and second coordinate are necessary). */
  errno=0; p->a=malloc(2*p->ar*sizeof *p->a);
  if(p->a==NULL) allocerror(p, 2*p->ar*sizeof *p->a, "p->a", errno);
  errno=0; p->b=malloc(2*p->br*sizeof *p->b);
  if(p->b==NULL) allocerror(p, 2*p->br*sizeof *p->b, "p->b", errno);


  /* Fill in the desired columns into allocated arrays, then free them
     since we don't need them any more. */
  for(i=0;i<p->ar;++i)
    {
      p->a[i*2  ] = fulla[i*ac+p->axcol];
      p->a[i*2+1] = fulla[i*ac+p->aycol];
    }
  for(i=0;i<p->br;++i)
    {
      p->b[i*2  ] = fullb[i*bc+p->bxcol];
      p->b[i*2+1] = fullb[i*bc+p->bycol];
    }


  /* Allocate the arrays to keep the sorted indexs (permutation) of
     each input catalog. Without these, after sorting we would have no
     idea where each initial (un-sorted) object was to make a
     meaningful output for the user.*/
  errno=0; p->apermutation=malloc(2*p->ar*sizeof *p->apermutation);
  if(p->apermutation==NULL)
    allocerror(p, 2*p->ar*sizeof *p->apermutation, "p->apermutation", errno);
  errno=0; p->bpermutation=malloc(2*p->br*sizeof *p->bpermutation);
  if(p->bpermutation==NULL)
    allocerror(p, 2*p->br*sizeof *p->bpermutation, "p->bpermutation", errno);


  /* Find the sorted permutations for both arrays and apply them so
     both are */
  gsl_sort_index(p->apermutation, p->a, 2, p->ar);
  gsl_permute(p->apermutation, p->a, 2, p->ar);
  gsl_permute(p->apermutation, p->a+1, 2, p->ar);
  gsl_sort_index(p->bpermutation, p->b, 2, p->br);
  gsl_permute(p->bpermutation, p->b, 2, p->br);
  gsl_permute(p->bpermutation, p->b+1, 2, p->br);


  /* Allocate and clear (initialize to NULL) the `bina'. Note that
     `bina' has the number of rows of catalog a (since it has pointers
     to which records in catalog b are near which in catalog a). And
     vice-versa for `ainb' later.*/
  errno=0; p->bina=calloc(p->ar, sizeof *p->bina);
  if(p->bina==NULL) allocerror(p, p->ar*sizeof *p->bina, "p->bina", errno);


  /* Clean up */
  free(fulla);
  free(fullb);
}





void
matchcatalogs(struct params *p)
{
  float r;
  size_t ai, bi;

  /* First find all elements in `b' that are withing the acceptable
     distance to `a'. */
  fillbina(p);

  /* For each `b', find the `a' with the minimum distance. */
  rearange(p);

  /* We now have the matches, we just have to print them out while
     also correcting the row numbers based on the permutations. Note
     that the popping is also going to free the allocated space for
     the node, otherwise we could have just read the values, because
     when present, there is only one node.*/
  for(ai=0;ai<p->ar;++ai)
    if(p->bina[ai])
      {
	linkedlist_pop_from_sfll(&p->bina[ai], &bi, &r);
	printf("%-10lu%-10lu%g\n", p->apermutation[ai]+1,
	       p->bpermutation[bi]+1, r);
      }


  /* Clean up. */
  free(p->bina);
}




















/**********************************************************************/
/*****************          Main function          ********************/
/**********************************************************************/
int
main(int argc, char *argv[])
{
  struct params p;

  /* Set the input parameters */
  setparams(&p, argc, argv);

  /* Do the matching. */
  matchcatalogs(&p);

  /* Clean up and return. */
  free(p.a);
  free(p.b);
  free(p.apermutation);
  free(p.bpermutation);
  return EXIT_SUCCESS;
}
