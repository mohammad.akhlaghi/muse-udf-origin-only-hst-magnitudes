# Comparing the magnitudes of NoiseChisel and Rafelski+2015
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# Desired rows of Rafelski+2015
# ----------------------------
r15compfilt=f775w
r15comp=$(catdir)/r15comp
r15compcat=$(r15comp)/r15cat.txt
$(r15comp): | $(catdir); mkdir $@
$(r15compcat): $(r15cat) | $(r15comp)
	asttable $(r15cat) -h1 -c1 -c3 -c4 -cMAG_$(r15compfilt)          \
	         --ignorecase  > $@;





# RAW NoiseChisel catalog with Upper-limit magnitdues
# ---------------------------------------------------
#
# The initial catalogs that NoiseChisel creates do not have upper
# limit magnitudes, since it is unnecessary at that stage for all the
# filters/depths. So we'll just generate a new catalog for this
# comparison here that has upper-limit magnitudes.
r15compnccat=$(r15comp)/ncgoodcat.txt
$(r15compnccat): $(ncfdir)/udf_$(r15compfilt).fits                       \
	         $(ncrawcatdir)/udf_$(r15compfilt)_c.txt | $(r15comp)

        # Gnuastro 0.2.52-f620 (that is used in this pipeline) will
        # not measure upper limit magnitudes on clumps, only on
        # objects. As a result, we need to create a special objects
        # image where there is no negative values and all clumps have
        # a unique ID (in the clumps image, all clumps have a pixel
        # value of 1).
	astarithmetic $< $< 1 eq $< where -h2 -h2 -h1 -o$(r15comp)/tmp.fits
	astarithmetic $(r15comp)/tmp.fits $(r15comp)/tmp.fits 0 lt 0 where   \
	              -o$(r15comp)/clumpobj.fits --type=long
	rm $(r15comp)/tmp.fits


        # Generate the catalog:
	export GSL_RNG_TYPE="mt19937";                                       \
	export GSL_RNG_SEED=1498150728;                                      \
	zp=$$(reproduce/src/zeropoints.sh $(r15compfilt));                   \
	astmkcatalog $< --zeropoint=$$zp -o$(r15comp)/rawnc.txt --envseed    \
	             --objlabs=$(r15comp)/clumpobj.fits --objhdu=0           \
	             --upperlimitmag --upmask=$< --upmaskhdu=1
	rm $(r15comp)/clumpobj.fits


        # We want the ID, position, magnitude and magnitude error from
        # the raw NoiseChisel catalog and the upper limit from the
        # catalog we just made.
	awk '!/^#/{print $$NF}' $(r15comp)/rawnc.txt > $(r15comp)/up.txt
	awk '!/^#/{print }' $(ncrawcatdir)/udf_$(r15compfilt)_c.txt          \
	    > $(r15comp)/rest.txt
	paste $(r15comp)/rest.txt $(r15comp)/up.txt > $(r15comp)/ncup.txt
	rm $(r15comp)/up.txt $(r15comp)/rest.txt


        # Remove all objects whose magnitude is more (fainter) than
        # their upper-limit magnitude.
	awk '$$4<$$NF{print}' $(r15comp)/ncup.txt > $@




# Match NoiseChisel and Rafelski+2015
# -----------------------------------
r15magdiff = $(r15comp)/magdiff.txt
$(r15magdiff): $(r15compcat) $(r15compnccat) $(matchprogram)                  \
	       $(iconf)/r15-max-match-dist.mk

        # Find the indexs of the match
	$(matchprogram) $(r15compcat) $(r15compnccat) $(r15maxmatchdist)     \
                         > $(r15comp)/matchinds.txt;

        # Sort the matched R15 objects similar to the NoiseChisel
        # detections (since the NC detections are smaller).
	numncrows=$$(awk '!/^#/{ ++nrows;                                     \
	                  print $$0 > "$(r15comp)/ncnocomments.txt" }         \
	          END{print nrows}' $(r15compnccat));                         \
	awk 'NR==FNR{inds[$$1]=$$2}                                           \
	     NR!=FNR{out[inds[FNR]]=$$0}                                      \
	     END{for(i=1;i<='$$numncrows';++i) print out[i]}'                 \
	    $(r15comp)/matchinds.txt $(r15compcat) > $(r15comp)/r15innc.txt

        # Get the magnitude differences for each object:
	awk 'BEGIN{                                                           \
	       print "# Column 1: RAF_MAG [AB magnitude, f] Rafelski+2015 $(r15compfilt) magnitude";\
	       print "# Column 2: RAF_NC_DIFF [AB magnitude, f] R15 - NoiseChisel magnitude"} \
	     NR==FNR{ r15mags[NR] = NF>0 ? $$4 : "n" }                        \
	     NR!=FNR && r15mags[FNR]!="n" { d=r15mags[FNR]-$$4;               \
	        printf "%-8.3f%-8.3f\n", r15mags[FNR], d; }'                  \
	    $(r15comp)/r15innc.txt $(r15comp)/ncnocomments.txt > $@





# Standard deviation by sigma clipping
# ------------------------------------
#
# It was finally decided that there were too many plots in the paper
# and the 2D histogram of magnitude dispersion will not be used. So,
# instead, we will just suffice to reporting the standard deviation in
# one magnitude bin. In this rule, we will define the parameters.
#
# This rule will also make a histogram of the selected region to
# justify why these sigma-clipping parameters were chosen: The
# distribution is too skewed, so choosing a larger multiple of sigma
# to clip would keep too much of the out-liers.
rafsclipmagmin    = 27.25
rafsclipmagmax    = 27.75
rafsclipmultip    = 2
rafscliptolerance = 0.1
r15sclipstd = $(r15comp)/sigclip-diff.txt
$(r15sclipstd): $(r15magdiff)

        # Temporarily ignore this part until the 0.3 version because
        # `aststatistics' was introduced in Gnuastro after that
        # version.
	echo "0 0 0 0 0 0 0 0 0" > $@

        # Then the standard deviation of the actual magnitudes.
        #aststatistics $< --column=RAF_MAG --greaterequal=$(rafsclipmagmin)  \
        #     --lessthan=$(rafsclipmagmax)                                   \
        #     --sigmaclip=$(rafsclipmultip),$(rafscliptolerance)             \
        #     > $(r15comp)/sigclip-orig.txt;

        # The standard deviation of the difference in this range.
        #aststatistics $< --column=RAF_NC_DIFF --refcol=RAF_MAG              \
        #     --greaterequal=$(rafsclipmagmin) --lessthan=$(rafsclipmagmax)  \
        #     --sigmaclip=$(rafsclipmultip),$(rafscliptolerance) --histogram \
        #     --numbins=50 --output=$(r15comp)/r15magbindiff.txt > $@

        # Plot the histogram to check/justify the choice of
        # sigma-clipping parameters.
        #python reproduce/src/plot-mag-diff-in-bin.py                        \
        #       $(r15comp)/r15magbindiff.txt $(r15comp)/r15magbindiff.pdf





# 2D histogram of the magnitude difference dispersion
# ---------------------------------------------------
r152dhistxmin = 24
r152dhistxmax = 31
r152dhistymin = -0.8
r152dhistymax = 0.8
r152dhistnbins = 60
r15compdir = $(texdir)/r15comp2dhist
r15comp2dhist = $(r15compdir)/r15comp2dhist.txt
$(r15compdir): | $(texdir); mkdir $@
$(r15comp2dhist): $(r15magdiff) | $(r15compdir)

        # Set the range and number of bins, then build the histogram.
	awk -v xmin=$(r152dhistxmin)      -v xmax=$(r152dhistxmax)            \
	    -v ymin=$(r152dhistymin)      -v ymax=$(r152dhistymax)            \
	    -v xnumbins=$(r152dhistnbins) -v ynumbins=$(r152dhistnbins)       \
	    -i reproduce/src/checks.awk   -f reproduce/src/2dhist.awk $< > $@





# Write in LaTeX
# --------------
$(mtexdir)/r15-comp.tex: $(iconf)/r15-max-match-dist.mk $(r15magdiff)         \
                         $(r15sclipstd)
	echo "\\newcommand{\\rafcompxmin}{$(r152dhistxmin)}"             > $@
	echo "\\newcommand{\\rafcompxmax}{$(r152dhistxmax)}"            >> $@
	echo "\\newcommand{\\rafcompnbins}{$(r152dhistnbins)}"          >> $@
	echo "\\newcommand{\\rafcompfilt}{\uppercase{$(r15compfilt)}}"  >> $@

	num=$$(awk 'END{print NR}' $(r15magdiff));                            \
	echo "\\newcommand{\\rafcompmatchnum}{$$num}"                   >> $@

	md=$$(echo $(r15maxmatchdist) | awk '{print $$1*3600}');              \
	echo "\\newcommand{\\rafcompmatchdist}{$$md}"                   >> $@

	val=$$(echo "$(rafsclipmagmin) $(rafsclipmagmax)"                     \
               | awk '{print ($$2+$$1)/2}');                                  \
	echo "\\newcommand{\\rafcompbincenter}{$$val}"                  >> $@

	val=$$(echo "$(rafsclipmagmin) $(rafsclipmagmax)"                     \
               | awk '{print ($$2-$$1)/2}');                                  \
	echo "\\newcommand{\\rafcompbinpm}{$$val}"                      >> $@

	echo "\\newcommand{\\rafcompscmultip}{$(rafsclipmultip)}"       >> $@
	echo "\\newcommand{\\rafcompsctolerance}{$(rafscliptolerance)}" >> $@
