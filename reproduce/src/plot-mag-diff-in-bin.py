import sys
import numpy as np
import matplotlib.pyplot as plt

hist=np.loadtxt(sys.argv[1])

plt.bar(hist[:,0], hist[:,1], width=hist[1,0]-hist[0,0])

plt.xlabel("Magnitude difference")
plt.ylabel("Number")
plt.savefig(sys.argv[2])
