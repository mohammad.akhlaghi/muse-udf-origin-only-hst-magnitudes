# Create the final segmentation map
#
# This Makefile is indented to be included by a Makefile that is in
# the top directory of this script.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This Makefile is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This Makefile is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details. See
# <http://www.gnu.org/licenses/>.





# ======================
# Final segmentation map
#
# First, we will match all NoiseChisel detections in all the filters
# with the given coordinates. Then the match in any filter with the
# largest detection is pulled out from the segmentation map and put
# into one image. For those objects that had no match, we will draw an
# aperture so every object has its own region in the image, ready to
# generate a final catalog.
# ======================

# Compile the matching program
# ----------------------------
#
# Matching is written in C for efficiency and ease of reading.
compiled=$(BDIR)/compiled
$(compiled): | $(BDIR); mkdir $@
matchprogram=$(compiled)/matchcats
$(matchprogram): reproduce/src/matchcats.c | $(compiled)
	gcc -O3 $< -lgnuastro -lgsl -lgslcblas -lm -o$@





# Match the separate NoiseChisel detections to the input catalog
# --------------------------------------------------------------
#
# We have the separate NoiseChisel detections on each filter, we also
# have the catalog of the MUSE detected objects. The purpose here is
# to find the row numbers of these two catalogs that match with each
# other.
matched = $(nccatdir)/matched
matchncinds = $(foreach f, $(allfilters), $(matched)/udf_$(f)_inds.txt)     \
              $(foreach f, $(xdfwfc3irf), $(matched)/xdf_$(f)_inds.txt)
$(matched): | $(nccatdir); $(nccatdir); mkdir $@
$(matchncinds): $(matched)/%_inds.txt: $(ncrawcatdir)/%_c.txt $(cat)        \
	        $(matchprogram) ./$(iconf)/max-match-dist.mk | $(matched)
	$(matchprogram) $(cat) $(ncrawcatdir)/$*_c.txt $(maxmatchdist) > $@





# Matched NC in order of input catalog
# ------------------------------------
#
# Pull out the necessary NoiseChisel catalog rows in the same order as
# the reference catalog, when there isn't any match, put a row of NaN
# values.
matchnc = $(foreach f, $(allfilters), $(matched)/udf_$(f).txt)     \
          $(foreach f, $(xdfwfc3irf), $(matched)/xdf_$(f).txt)
$(matchnc): $(matched)/%.txt: $(matched)/%_inds.txt

        # Here we are using AWK's array functionality (where the index
        # is its self a string and there is no particular order). On
        # the first file, NR==FNR and the first file we are giving to
        # AWK is the matched indexs. To remove the problem of comments
        # in counting line-numbers, we first remove all the comments
        # from NoiseChisel's output.
	ncat=$$(wc -l $(cat) | awk '{print $$1}');                           \
	awk '!/^#/{print}' $(ncrawcatdir)/$*_c.txt                           \
	    > $(matched)/$*_nocomment.txt;                                   \
	awk -v ncat=$$ncat '                                                 \
           NR==FNR{ outinds[$$1]=$$2 }                                       \
           NR!=FNR{ for(i in outinds) if(FNR==outinds[i]) out[i]=$$0; }      \
	   END{ for(i=1;i<=ncat;++i)                                         \
                  if(out[i]=="")                                             \
                    {                                                        \
                      for(j=0;j<NF;++j) printf "%s ", "nan";                 \
	              printf "\n";                                           \
                    }                                                        \
                  else                                                       \
                      print out[i] }'                                        \
	   $(matched)/$*_inds.txt $(matched)/$*_nocomment.txt > $@
	rm $(matched)/$*_nocomment.txt





# Make the overall matched catalog
# --------------------------------
#
# One catalog that keeps the IDs and sizes of all NoiseChisel
# detections in all filters that correspond to the input catalog.
allncmatch=$(matched)/all.txt
$(allncmatch): $(matchnc)

        # Make a blank base file to keep the data from each filter.
	rm -f $@.t
	touch $@.t

        # Add the ID and area of each NC catalog's output.
	for filename in $(matchnc); do                                      \
	  awk '$$1=="nan"{printf "%-6s%-6s\n", "nan", "nan"}                \
               $$1!="nan"{printf "%-6d%-6d\n", $$1, $$6}' $$filename > $@.f;\
          paste -d' ' $@.t $@.f > $@.p;                                     \
	  mv $@.p $@.t;                                                     \
          rm $@.f;                                                          \
	done;

        # Move the temporary output to the target of this rule. We did
        # not build in the target initially so if an error happens,
        # this rule is run.
	mv $@.t $@





# Find filter with largest NC detection
# -------------------------------------
#
# Having found the matched catalog, find the best filter for each
# detection. Note that the starting ID `startingid' comes from a
# configuration file.
bestfilter=$(matched)/bestfilter.txt
$(bestfilter): $(allncmatch) $(iconf)/starting-id.mk

        # Find the best field, filter and ID for each object.
	awk 'BEGIN{split("$(allfilters) $(xdfwfc3irf)", filters, " ");}    \
             {                                                             \
               maxarea=0;                                                  \
               for(i=0;i<$(words $(allfilters) $(xdfwfc3irf));++i)         \
                {                                                          \
	          ind=i*2+1;                                               \
	          if($$ind!="nan" && $$(ind+1)>maxarea)                    \
                    { maxarea=$$(ind+1); filt=filters[i+1]; id=$$ind;      \
	              surv = i+1<=$(words $(allfilters)) ? "udf" : "xdf"; }\
                };                                                         \
	       if(maxarea==0)                                              \
	         printf "%-6s%-8s%-6s%-6s\n", "nan", "nan", "nan", "nan";  \
	       else                                                        \
	         printf "%-6s%-8s%-6d%-6s\n", surv, filt, maxarea, id;     \
             }' $(allncmatch) > $@.t

        # Merge the reference catalog and the matches
	paste -d' ' $(cat) $@.t > $@

        # Clean up
	rm $@.t





# All pixel labels into one image
# -------------------------------
#
# In the bestfilter step, we found the filters and object/clump IDs in
# those filters of the best NoiseChisel detections for each
# object. The problem is that those pixel labels are in separate
# images. So here we pull out the desired pixels of each object from
# the respective image onto a blank image and give them the new values
# we want here. This will make it easy to simply use that image in
# MakeCatalog.
ncmatchedseg=$(segdir)/noisechisel.fits.gz
$(ncmatchedseg): $(bestfilter) .gnuastro/astarithmetic.conf | $(segdir)

        # First make a blank field from one of the input images. Note
        # that no value equals NaN, so the output is going to be an
        # "all 0 image).
	astarithmetic $(ncfdir)/udf_f775w.fits nan eq            \
	              -o$(segdir)/tobefilled.fits.gz

        # Bring in each object's pixels from the best filter. Note
        # that most of the pixels in the images here are single
        # values, enabling great compression rates. So to reduce the
        # strain on the non-volatile (HDD or SDD) drives, we are
        # keeping the temporary files as tar.gz. CFITSIO and DS9 can
        # both open .tar.gz files, so this will just slightly decrease
        # the speed, but has no other side effects.
	while read line; do                                                 \
	  inid=$$(echo $$line | awk '{print $$8}');                         \
	  outid=$$(echo $$line | awk '{print $$1}');                        \
	  filter=$$(echo $$line | awk '{print $$6}');                       \
	  field=$$(echo $$line | awk '{print $$5}');                        \
          if [ $$filter = "nan" ]; then continue; fi;                       \
	  ops="$$ops $(ncfdir)/$$field"_"$$filter.fits $$inid eq -h1 ";     \
	  ops="$$ops $(ncfdir)/$$field"_"$$filter.fits 1      eq -h2 and "; \
	  ops="$$ops $$outid where";                                        \
	done < $(bestfilter);                                               \
	echo "Writing all NoiseChisel labels into one segmentation map.";   \
	astarithmetic $(segdir)/tobefilled.fits.gz -h0 $$ops                \
	              -o$(segdir)/arith_tmp.fits.gz;
	mv $(segdir)/arith_tmp.fits.gz $(segdir)/tobefilled.fits.gz;

        # Convert the image to long type so it can easily be read into
        # MakeCatalog, we didn't do it above on each separate
        # invocation of astarithmetic because astarithmetic converts
        # them internally to double, and so we would have two
        # unnecessary conversions each time it was run.
	astarithmetic $(segdir)/tobefilled.fits.gz 0 + -o$@ --type=long
	rm $(segdir)/tobefilled.fits.gz





# Make the catalog to create the aperture segmentation maps
apercat = $(catdir)/apertures-cat.txt
aperturefits=$(segdir)/apertures.fits.gz
$(apercat): $(bestfilter) $(cat) $(iconf)/aperture.mk

	awk 'NR==FNR{rows[NR] = $$5=="nan" ? 1 : 0}                          \
	     NR!=FNR{                                                        \
	      if(rows[FNR]==1)                                               \
	       print($$1, $$2, $$3, 4, $(apertureradius), 1, 0, 1, $$1, 1)}' \
	    $(bestfilter) $(cat) > $@





# Draw the apertures of objects not detected by NoiseChisel over the
# NoiseChisel segmentation map.
ncandaper = $(segdir)/nc-and-aper.fits.gz
$(ncandaper): $(ncmatchedseg) $(apercat) .gnuastro/astmkprof.conf | $(segdir)
	astmkprof $(apercat) $(ncmatchedseg) --mforflatpix --replace         \
	          --oversample=1 --racol=1 --deccol=2 --type=long  -o$@





# Draw the apertures over the NoiseChisel segmentation map
finalseg = reproduce/output/hst-raf-with-origin.fits.gz
$(finalseg): $(ncandaper) $(r15seg) .gnuastro/astimgcrop.conf                \
	     .gnuastro/astimgwarp.conf | reproduce/output

        # Warp the pixel grid to double the size for the same
        # resolution as Rafelski's catalog.
	astimgwarp $(ncandaper) --scale=2 --doubletype -o$(segdir)/warped.fits

        # Add the pixels to the Rafelski segmentation map.
	astarithmetic $(r15seg)                                             \
	              $(segdir)/warped.fits 0 gt $(segdir)/warped.fits 4 '*'\
	              where --type=long -o$(segdir)/finalcat.fits

        # Compress the segmentation map and save it. This style of
        # compressing appears to give a better compression ration than
        # CFITSIO doing the compression internally
	gzip --stdout $(segdir)/finalcat.fits > $@

        # Clean up
	rm $(segdir)/warped.fits $(segdir)/finalcat.fits





# Write the necessary TeX macros
$(mtexdir)/final-seg.tex: reproduce/config/internal/aperture.mk          \
	                  reproduce/config/internal/pixel-scale.mk       \
	                  reproduce/config/internal/max-match-dist.mk    \
	                  | $(mtexdir)

        # The matching distance is in degrees, so we have to convert
        # it to arcseconds for easy reading in the paper.
	out=$$(echo $(maxmatchdist) | awk '{printf "%.2f", $$1*3600}');  \
	echo "\\newcommand{\\maxmatchdist}{$$out}"                > $@

        # Aperture diameter in arcseconds. Note that internally, it is
        # stored as pixels and the radius is stored, not the
        # diameter. Also note that the pixel scale is just an integer
        # so we have to convert it to arcseconds/pixels by dividing it
        # by 100.
	out=$$(echo "$(pixelscale) $(apertureradius)"                    \
	       | awk '{printf "%.2f", 2*$$1*$$2/100}' );                \
	echo "\\newcommand{\\aperturediameter}{$$out}"             >> $@
