%% extarticle is part of the extsizes package, which provides extended
%% sizes for the standard classes.
\documentclass{extarticle}

%% If you want to use PDF files (and not build them with TikZ), then
%% comment the next line. To use PDF files, they have to be in a
%% `tikz' directory. To MAKE PDF files with TikZ and PGFPlots, it is
%% assumed that the texfile containing the source are in a `tex'
%% subdirectory relative to the directory which latex is run in.
\newcommand{\makepdf}{}

%% To high-light notes/changes, uncomment this macro:
\newcommand{\highlightchanges}{}

%% Input the style dependent and necessary (style independent)
%% preamble.
\input{tex/preamble-style.tex}
\input{tex/preamble-biblatex.tex}
\input{tex/preamble-necessary.tex}





%% Start the main body
\begin{document}

%% Set the two column structure and build the title.
\twocolumn[
  \begin{@twocolumnfalse}
    {\setstretch{1.0}
      \maketitle
    }
  \end{@twocolumnfalse}
]
\setlength{\leftskip}{0pt}
\setlength{\rightskip}{0pt}

%% With the ulem package, marked text will be broken into separate
%% lines. However, it will also cause the `\emph' command to underline
%% the text, instead of making it italic. With this command, `\emph'
%% will behave normally.
\normalem


\section{Section 7.3 supplement}
Accurate derivation of a target's spectrum, and emission line
equivalent widths over the wide MUSE 3D cubes requires the target's
spatial footprint (segmentation map) in broad-band images and also
their broad-band magnitudes. The UVUDF survey \citep[hereafter
  R15]{rafelski15} catalog and segmentation map were the primary
resource for this information. However, the wide field of MUSE offers
the ability to detect targets based on spectral features for the first
time.  Therefore, in parallel to the R15 catalog, an independent 3D
detection procedure was also applied directly to the MUSE datacube
using the \originsoft{} software.

\tonote{What was the total number of \originsoft{}-only detections? In
  comparison with the \numnotinraf{} that were not in R15, this would
  be interesting completeness study and we might also check with
  \noisechisel{} generated catalogs.}

The two independent catalogs were then matched \tonote{we need to add
  the search radius for the matching of \originsoft{} results with R15
  and possibly some other details, if it wasn't discussed in the
  section on \originsoft.}. However, \numnotinraf{} of the
\originsoft{} detections did not corresponded to anything in the UVUDF
catalog. Therefore, in Section \ref{apr15-withfootprint} the list of
objects is classified into various classes and in Section
\ref{apr15-derivation} the procedure to derive the segmentation maps
and broad-band magnitudes for this list is discussed. All the results
in this appendix are exactly reproducible and the reproduction
pipeline is released with this paper\footnote{\tonote{Similar to
    \citet{noisechisel} where the reproduction pipeline is uploaded to
    arXiv along with the final paper's \TeX{} sources. arXiv is the
    most reliable online place to store them with the paper (the whole
    pipeline is $<200$kB). The pipeline can also be submitted to the
    journal.} The version controlled reproduction pipeline of this
  appendix is also available at
  \url{https://gitlab.com/makhlaghi/muse-not-in-rafelski15}.}.





\subsection{Classification}\label{apr15-withfootprint}

An accurate analysis of the \originsoft-only objects is necessary to
confirm whether the absence of these objects in the R15 broad-band
catalog is an artifact of the detection methods and configurations
used, or it is indeed due to very weak (possibly non-existant)
continuum emission. Given this independently derived list of objects
(from the fundamentally different MUSE datacubes and different
algorithm), this analysis can be very useful in analysing the causes
of success or failure of the detection method used.

The objects can generally be classified into two classes: 1) Those
with a footprint in the R15 segmentation map, but not in the catalog
(Figure \ref{r15prob}). 2) Those without a footprint in the R15
segmentation map (Figure \ref{r15other}). The R15 segmentation map and
catalog was derived by applying \sextractor{} \citep{sextractor} to
the average of all optical and near-infrared images and is a merger of
several \sextractor{} runs with multiple detection and deblending
paramters (see Table 2 or R15). Since these objects don't exist in the
R15 catalog, a visual analysis was necessary, so
$\rafprobwwidth^{\prime\prime}$ cutouts were made (centered on the
object) from the \rafprobimgbb{} image and the R15 segmentation map.

To understand the first class, it is important to recall that given
the particular detection algorithm and input parameters used, non-zero
pixels in a segmentation map correspond to the \emph{detected}
pixels. However, partial or full coverage (non-zero pixels) of the
object's pixels in the segmentation map does not guarantee the
object's existance in the catalog (as a unique object/row).

Figure \ref{r15prob} shows three categories of the first class of
objects: 1.1) Those due to problems in deblending at a distance (first
row). 1.2) Those due deblending problems of nearby objects (second
row). 1.3) Those that were removed manually based on S/N and number of
detections for high purity (third row). R15's primary reason for
merging multiple \sextractor{} catalogs was to correct for the 1.2
category debleding problems (second row of Figure
\ref{r15prob}). However we see that $\rafprobrc\%$ of the
\originsoft-only detections belong to this class. This failure is
caused by a fundamental systematic bias in \sextractor's deblending
methodology and has been thoroughly discussed in Appendix B.2 of
\citet[hereafter AI15]{noisechisel}. The reasons for the first row of
Figure \ref{r15prob} are also described there.

\begin{figure*}[t]
  \centering
  \ifdefined\makepdf
  \input{./tex/r15prob}
  \else
  \includegraphics[width=\linewidth]{./tikz/paper-figure0.eps}
  \fi
  \caption{\label{r15prob} First class of \originsoft{} detected
    objects that did not correspond to objects in the R15 catalog, but
    with footprints in the R15 segmentation map. The cutouts are
    $\rafprobwwidth^{\prime\prime}$ wide and centered on the
    \originsoft{} detections. The first column for each object (a.1,
    b.1, c.1, d.1, e.1 and f.1) are cutouts from the \rafprobimgbb{}
    image. The second column shows the non-zero (corresponding to an
    object) pixels in the R15 segmentation maps. The third column
    shows the \noisechisel{} detections in the \rafprobimgbb{} image
    in gray and the black pixels show the final pixels used for this
    object (see Section \ref{apr15-derivation}). The first row (a and
    b) are spatially separated from other objects, nevertheless they
    have the same label as the object shown by the red arrow (also see
    Figure 17 of AI15). The second row (c and d) were too close to a
    much brighter object and thus not labeled differently. The third
    row (e and f) are objects with a unique label, but trimmed from
    from the R15 catalog for high purity (Section 5 in R15). These
    three classes constitute approximately $\rafprobrd\%$,
    $\rafprobrc\%$, and $\rafprobrs\%$ of the \originsoft{}-only
    detections respectively.}
\end{figure*}

\FPeval{\originonlyfaint}{clip(\rafprobns+\rafprobf)} Figure
\ref{r15other} shows the second class of objects, the three categories
for this class are: 2.1) Those that were detected by \noisechisel{} in
at least one of the filters (see Section \ref{apr15-derivation}). 2.2)
Those where the diffuse flux was detected in the \rafprobimgbb{}
filter, but no `clump' could be specified for it. 2.3) Those that
could not be detected by \noisechisel{}. From Figures \ref{r15prob}
and \ref{r15other}, it is clear that only $\originonlyfaint\%$ of the
\originsoft{} detected objects truely had very low continuum surface
brightness to be detected as a separate (non-diffuse) entity. As
reviewed above, this shows the methodological biases against
reasonable completeness in R15's detection tools/parameters.

Note that $\rafproboutside$ object (MUSE ID $6449$) was outside of the
field in the F775W images we were inspecting here for the
classification, so it is not included in any of the percentiles
mentioned above.

\begin{figure*}[t]
  \centering
  \ifdefined\makepdf
  \input{./tex/r15other}
  \else
  \includegraphics[width=\linewidth]{./tikz/paper-figure1.eps}
  \fi
  \caption{\label{r15other} Similar to Figure \ref{r15prob}, but for
    objects that were not covered by the R15 segmentation map. The
    first row (a and b) are objects that had a \noisechisel{}
    detection. The second row are objects that were within
    \noisechisel's detected regions, but could not be identified as a
    clump (their flux is blended too strongly with neighboring
    objects). The third row are example objects that could also not be
    detected by \noisechisel{}. These three categories constitute
    approximately $\rafprobnc\%$, $\rafprobns\%$ and $\rafprobf\%$ of
    the \originsoft{}-only detections respectively.}
\end{figure*}


\subsubsection{Corrections after publication}

After the publication of \citep{bacon17} and when preparing this
pipeline for publishing, some very minor differences were found
between this output and what was finally finished in the paper. As
shown below, these differences are very minor and have no effect on
our final result, but since the aim of this pipeline is \emph{exact}
reproduction, the differences must be described.

In the published results, the classification for object $6477$ (MUSE
ID) was not typed correctly and thus it was not included in the
percentiles. This typo is now corrected. Finally, the percentiles in
the published version were based on an input list of 173 objects. But
that list was later (after giving the final results of this pipeline,
and prior to publication) corrected to 160. It was my (MA)
responsability to notice the change in the paper, but since it was a
small change in the whole paper, I missed it and thus didn't update
the pipeline's input or report a change in the output. Fortunately it
only has a small effect on the percentiles. Therefore, it has no
effect on the conclusions drawn from the percentiles. The input list
in the pipeline now is the final list of 160 objects.

Another minor mistake found in the published paper is in the third
paragraph of Section 7.3, we mentioned that ``We inspect the 88
HST-detected objects discussed above ...''. However, the number 88
(160 subtracted by the 72 aperture-based detection criteria) was a
mistake. The processing that resulted in 72 objects was done with
fixed apertures for all the objects and it was independent of this
classification. Not noticing the new number 88 in this paragraph was
my (MA) failure. This paragraph in the paper is about objects that
were covered by a \sextractor{} segmentation map (the first class
discussed in Section \ref{apr15-withfootprint} above, Figure
\ref{r15prob}). From the percentiles of Figure \ref{r15prob}, we see
that this constitues $\rafprobinseg$ objects. So the first line of
this paragraph must have been ``We inspect the $\rafprobinseg$
ORIGIN-only objects that were covered by a detection in the
\citet{rafelski15} segmentation map ...''. Thefore, the three classes
of Figure \ref{r15prob} constitute $\rafprobrdinrseg\%$,
$\rafprobrcinrseg\%$ and $\rafprobrsinrseg\%$ of the $\rafprobinseg\%$
and these numbers are very close to those mentioned in the paper
(namely $8\%$, $73\%$ and $15\%$).

Finally, as noted in Figure \ref{r15other}, following all the
corrections above, the value $39\%$ that is discussed in the last
paragraph of this section in the published paper should have been
$\rafprobnc\%$. In the period after reporting these results, I (MA)
was heavily involved in the development of Gnuastro for our next
projects with MUSE. Also, the concept behind this reproduction
pipeline is also evolving very rapidly. These might have been partly
the cause for these very minor mis-matches between this pipeline's
results and those of the paper. In short these were human error and
the future pipelines will be designed to mitigate the sources of this
as much as possible.



\subsection{Derivation of segmentation maps and broad-band magnitudes}\label{apr15-derivation}

The input $\pixelscale^{\prime\prime}$/pixel broad-band images were
taken from the publicly available XDF\footnote{\url{\xdfweb}}
\citep[for the optical and near infra-red images]{XDFsurvey} and
UVUDF\footnote{\url{\uvudfweb}} (R15, for the near UV images)
surveys. In the IR images, the public XDF release is a multi-depth
image containing the very deep single WFC3-IR field of view with the
rest of the UDF area covered by shallower images. See Figures 3 and 4
in \citet{XDFsurvey} for the optical and IR exposure maps over the
UDF.

Accurate detection and photometry require an approximately flat
depth/exposure (or noise). Therefore, in the infrared, the deep and
shallow regions were cutout and the full procedure below was done
independently on each region\footnote{The south, east, north and west
  vertices of the UDF field are respectively chosen to be at
  ($\udfraa$, $\udfdeca$), ($\udfrab$, $\udfdecb$), ($\udfrac$,
  $\udfdecc$), and ($\udfrad$, $\udfdecd$). This area covers the full
  R15 segmentation map. The vertices of the deep infrared XDF region
  are defined in \url{https://archive.stsci.edu/prepds/xdf} and
  located at ($\xdfraa$, $\xdfdeca$), ($\xdfrab$, $\xdfdecb$),
  ($\xdfrac$, $\xdfdecc$), and ($\xdfrad$, $\xdfdecd$). The
  deep/shallow boundary region with strong gradients is outside the
  deep infrared XDF region but inner to these vertices: ($\lxdfraa$,
  $\lxdfdeca$), ($\lxdfrab$, $\lxdfdecb$), ($\lxdfrac$, $\lxdfdecc$),
  and ($\lxdfrad$, $\lxdfdecd$).}. Unfortunately the XDF survey has
merged the shallow and deep regions into one image, therefore roughly
1/10th of the UDF area is covered by a very strong exposure/noise
gradient. $\fracovergradir\%$ of the \originsoft{}-only objects lie on
this gradient region. They have been flagged to note that their
infrared magnitude error measurements are unreliable.  \tonote{Discuss
  how R15 deals with this multi-depth region.} \tonote{Currently the
  images are not PSF-matched.}

As discussed above, approximately $\sim\rafprobrc\%$ of the objects
lie very close to a nearby neighbor along the line of sight in the R15
segmentation map (second row in Figure \ref{r15prob}). With the much
lower (sub-Sky) thresholds of \noisechisel, this number greatly
increases. For example compare the gray regions of third column in
Figures \ref{r15prob} and \ref{r15other} with the black regions of the
second columns. Most commonly these much larger and brighter neighbors
are lower redshift galaxies, with no physical proximity to our desired
target. To avoid the effect of the neighbor's diffuse and large wings
on the shape and thus measured photometry of these objects we use
\noisechisel{}'s ``clumps'' to identify their pixels (see Section
3.2.1 of AI15). The boundaries of clumps in \noisechisel{} are defined
by the 2D noise fluctuations and therefore locate the region were the
diffuse neighbor's wings start significantly influencing the total
pixel flux.

When there is no nearby neighbor, the diffuse flux (that has been
detected) can unambiguously\footnote{`Unambiguous' is used in
  reference to the particular image's depth and resolution. Much
  deeper and higher resolution images (for example by JWST) might
  reveal substructure.}  be used in the object's photometry. To
implement this, the following strategy is adopted: when a detection is
covered by more than one clump, the clump's segmentation map is
used. However, when the detection has only one clump or no clumps at
all (a very diffuse object), we use the full detection's segmentation
map for that object\footnote{The following \noisechisel{} options were
  set to achieve this behavior: \texttt{\small -{}-grownclumps},
  \texttt{\small -{}-gthresh\-=\ncgthresh}, \texttt{\small
    -{}-objbordersn\-=\ncobjbordersn}.}. For more on the distinction
between detections and clumps, please see Sections 3.1 and 3.2 of
AI15\footnote{As a review, in the signal-based paradigm (which was
  exclusively used in astronomy until now) peaks are first found and
  used to estimate/model the fainter parts that are buried in
  noise. So there is no distinction between (true) peaks and
  detections. However, in the noise-base paradim, a detected region is
  first found using a threshold that is far below the Sky value. The
  detected region is hence found independently of the brightness
  structure above the Sky value. Afterwards, true peaks (``clumps'')
  are found over the detected region. Thus a detection region can have
  no peaks.}. Finally, since the existance of these objects is known
a-priori (independent of these broad-band images, in other words,
there is no purity problem) we have set \texttt{\small
  -{}-detquant\-=\ncdetquant}, and \texttt{\small
  -{}-segquant\-=\ncsegquant} to increase completeness. \tonote{Add
  the \noisechisel{} configuration file.}

Figure \ref{r15comp2dhist} shows the magnitude difference between R15
and our run of \noisechisel{} (with the configuration discussed above)
on the \rafcompfilt{} image. In the case of brighter objects, R15 has
measured a brighter magnitude (bottom half mostly filled) for some
objects compared to \noisechisel{}. This confirms the success of the
configuration discussed above: diffuse flux is more common around
large/brighter objects. By ignoring the diffuse flux when there is
more than one clump (to detect and measure the magnitudes of the first
row in Figure \ref{r15prob}), we measure a fainter magnitude. The
deblending problems of \sextractor{} near bright objects also
contributes to this brighter magnitude. For the fainter objects, it is
clear how at fainter limits, we have detected a brighter magnitude
compared to R15. This is due to \noisechisel's exceptional ability to
dig deeply into the noise and detect the fainter flux.  Compare the
sizes of the regions between the second and third columns of Figures
\ref{r15prob} and \ref{r15other}. \tonote{Currently the labeled
  segments in the final segmentation map are treated as objects (even
  if they were clumps originally) so the flux of the river pixels
  around the clumps is not subtracted. To be very precise we have to
  subtract the river pixel's flux from the clumps. }

To obtain the magnitudes of the \originsoft-only (input) detected
objects, \noisechisel{} is run with this configuration on all the
filters individually. The resulting catalog is then matched with the
input catalog. Matching is defined as the nearest object within
$\maxmatchdist^{\prime\prime}$ of the input catalog. The largest
segmentation map (from all the filters that have a match) is used to
represent the spatial footprint of each input object. If an object
could not be matched with a detection on any of the filters, an
aperture with a diameter of $\aperturediameter^{\prime\prime}$ is
positioned on the given coordinate. This gives a uniquely labeled
region for each \originsoft-only detected object which are then merged
into one segmentation map. \tonote{Due to the narrower PSF of the ACS
  filters, ideally the segmentation map of ACS filters needs to be
  convolved with a matching kernel to be used on WFC3 filters.}

The segmentation map is fed into \makecatalog{} along with the input,
Sky, and $\sigma_{Sky}$ of each filter to generate the final
catalog. \tonote{Currently the Sky and $\sigma_{Sky}$ images are taken
  from the first run of \noisechisel{} on each image. We need to add
  an extra run tailored for an accurate Sky and $\sigma_{Sky}$
  measurements.} \tonote{Also, the images are not currently
  PSF-matched, so the unified segmentation map is not measuring
  exactly the same area of the sky. Ideally, we should use one
  segmentation map from ACS and WFC3/UV images and one for WFC3/IR
  images.}

\begin{figure}[t]
  \centering
  \ifdefined\makepdf
  \input{./tex/r15comp2dhist}
  \else
  \includegraphics[width=\linewidth]{./tikz/paper-figure2.pdf}
  \fi
  \caption{\label{r15comp2dhist} Magnitude difference as a
    function of magnitude, for \rafcompmatchnum{} objects in
    \citet{rafelski15} that are within
    $\rafcompmatchdist^{\prime\prime}$ of our \noisechisel{} run's
    detections.}
\end{figure}

As lower magnitudes are measured, the observational scatter caused by
the noise can greatly harm the robustness of the result. Therefore
when an object's magnitude is below a certain limit, the magnitude is
replaced with an upper limit magnitude. Given the different
(correlated) noise properties of each image and very large range of
sizes and shapes that are found or set for the objects, a fixed value
cannot be used as an upper limit magnitude for all the
objects. Therefore we also call \makecatalog{}'s
\texttt{-{}-upper\-limit\-mag} column in this run. When asked for this
column, \makecatalog{} will take the object's segmentation map and
shift it to various random positions over the undetected areas of the
image and will measure the magnitude until \mkcatupnum{} magnitudes
are measured for that object over the blank areas of the image. The
upper limit magnitude for that object is then taken as a multiple
($\mkcatupnsigma$ in this case) times the standard deviation of this
distribution\footnote{To decrease the effect of strong outliers, the
  upper limit is actually found after $\sigma$-clipping: all objects
  beyond $\mkcatupsclipmultip\sigma$ are clipped until the relative
  change in $\sigma$ is below \mkcatupsclipaccu. See Section A.3 in
  AI15.}. When the measured magnitude of the object is below this
limit, this upper limit magnitude is used instead.

\printbibliography
\end{document}
