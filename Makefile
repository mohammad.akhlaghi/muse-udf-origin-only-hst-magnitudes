# Photometry of MUSE detected targets not in the Rafelski Catalog.
# See README for a more complete description, and setup methods.
#
# Original author:
#     Mohammad Akhlaghi <mohammad@akhlaghi.org>
# Contributing author(s):
# Copyright (C) 2016-2018, Mohammad Akhlaghi.
#
# This script is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This script is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# A copy of the GNU General Public License is available at
# <http://www.gnu.org/licenses/>.





# First goal is the default
# -------------------------
all: description.pdf






# Configurations
# --------------
#
# These are the general configuration variables for a large number of
# independent rules, so they need to be included up here.

# Configuration parameters
include reproduce/config/internal/*.mk





# Rules
# -----
#
# Note that we cannot simply include `reproduce/src/*.mk'. Because the
# order of reading them into Make actually matters in some cases.
include $(foreach m,preparations download crop raw-cats final-seg	\
	            final-cat r15-mag-comp cutouts wht-aper-cat		\
	            description, reproduce/src/$(m).mk)





# Phony targets
# -------------
#
# These are targets that do not correspond to a file (phony in the
# Make terminology). The following rules define what they do.
.PHONY: all clean-all clean-pdf clean-version





# Clean rules
# -----------
#
# To clean the outputs if necessary.

clean-all:			# Clean everything.
	rm -rf $(BDIR) $(BSYM) reproduce/output tikz
	rm -f tex/pipeline.tex description.bbl description.aux description.pdf

clean-pdf:                      # Clean only the PDF-related outputs.
	rm -f $(mtexdir)/versions.tex
	rm -rf tikz $(texdir) *.pdf *.bbl *.aux

clean-version:			# Clean the version information.
	rm -f $(mtexdir)/versions.tex
